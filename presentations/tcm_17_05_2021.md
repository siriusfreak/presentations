---
title: "Лекция 17. Алгоритм Дойча. Язык Q#. Сложности алгоритмов и классы сложности."
block: "Теоретические модели вычислений"
order: 17
customTheme : "presentation"

---

### Теоретические модели вычислений. 
### Лекция 17. Алгоритм Дойча. Язык Q#. Сложности алгоритмов и классы сложности.

#### 17 мая 2021 года

---

### План занятия
1. Алгоритм Дойча
2. Язык Q# <!-- .element: class="fragment" -->
3. Суперпозиции на Q# <!-- .element: class="fragment" -->
4. Различение состояний на Q# <!-- .element: class="fragment" -->
5. Квантовые оракулы и алгоритм Дойча на Q# <!-- .element: class="fragment" -->
6. Понятие сложности алгоритмов <!-- .element: class="fragment" -->
7. Расчёт сложности алгоритмов <!-- .element: class="fragment" -->
8. Классы сложности алгоритмов <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Пусть есть функция $f(x): \\{0, 1\\} \rightarrow \\{0, 1\\}$.

Она принимает некоторое двоичное число и возвращает двоичное.  <!-- .element: class="fragment" -->

Требуется определить является ли функция константой (т.е. всегда возвращает одно и то же значение) или она сбалансирована (т.е. меняет возвращаемое значение в зависимости от входа). <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

В классическом мире нам требуется два вызова функции $f(x)$, чтобы понять её природу.

* $f(0) = f(1)$ - константа  <!-- .element: class="fragment" -->
* $f(0) \neq f(1)$ - сбалансирована <!-- .element: class="fragment" -->

Усложним задачу: пусть функция $f(x)$ вычисляется 29 лет. <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Алгоритм Дойча позволяет понять природу функции $f(x)$ за 1 вычисление. Он уже реализован и демонстрирует квантовое превосходство над классическими алгоритмами.<!-- .element: class="fragment" -->

Для того, чтобы эту функцию перевести в квантовый мир, необходимо для неё построить Оракул (так называется, потому что мы не знаем, как он работает, но при этом он нам возвращает нужные значения).  <!-- .element: class="fragment" -->

Построение оракула доказывает, что функция может быть облачена в унитарный оператор. <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Оракул: $U_f$

Для вышеописанной функции Оракул будет иметь следующий вид: <!-- .element: class="fragment" -->

$$U_f \Ket{x}\Ket{y} = \Ket{x}\Ket{y \oplus f(x)}$$ <!-- .element: class="fragment" -->

$x$ используется для хранения предыдущего значения. $y$ - производит вычисления функции $f(x)$.  <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Мы должны преобразовать это к обратимой форме (как принято в квантовом мире):

$U_f \Ket{x} \Ket{y \oplus f(x)} = \Ket{x} \Ket{y \oplus f(x) \oplus f(x)} = \Ket{x} \Ket{y}$ <!-- .element: class="fragment" -->

$U_f U_{f}^* = I$  $U_{f}^{-1} = U_f$ <!-- .element: class="fragment" -->

Он обартим. <!-- .element: class="fragment" -->


---

### Алгоритм Дойча

Проверим, как он воздействует на базисные векторы:

$U_f \Ket{0} \Ket{0} \rightarrow \Ket{0} \Ket{f(0)}$ <!-- .element: class="fragment" -->

$U_f \Ket{1} \Ket{0} \rightarrow \Ket{0} \Ket{f(1)}$ <!-- .element: class="fragment" -->

$U_f \Ket{0} \Ket{1} \rightarrow \Ket{0} \Ket{1 \oplus f(0)}$ <!-- .element: class="fragment" -->

$U_f \Ket{1} \Ket{1} \rightarrow \Ket{1} \Ket{1 \oplus f(1)}$ <!-- .element: class="fragment" -->



---

### Алгоритм Дойча

Пояснения:

$0 \oplus f(x) = 0 \oplus 0 = 0 = f(x)$ - если $f(x) = 0$ <!-- .element: class="fragment" -->

$0 \oplus f(x) = 0 \oplus 1 = 1 = f(x)$ - если $f(x) = 1$ <!-- .element: class="fragment" -->

$0 \oplus f(x) = 0 \oplus x = x = f(x)$ - если $f(x) = x$ - сбалансирована <!-- .element: class="fragment" -->

$0 \oplus f(x) = 0 \oplus \bar x = \bar x = f(x)$ - если $f(x) = \bar x$ - сбалансирована <!-- .element: class="fragment" -->

Таким образом,  $0 \oplus f(x) = f(x)$ всегда. Базисные вектора переходят в базисные, т.е. оператор сохраняет угол. <!-- .element: class="fragment" -->


---


### Алгоритм Дойча

В зависимости от того, является ли $f$ константой или нет, мы можем заметить, что может произойти склеивание. 

$$U_f \Ket{x} \frac {1}{\sqrt 2} (\Ket{0} - \Ket{1}) =$$

$$\frac {1}{\sqrt 2} \Ket{x} (\Ket{0 \oplus f(x)} - \Ket{1 \oplus f(x)}) = $$ <!-- .element: class="fragment" -->

$$\frac {1}{\sqrt 2} \Ket{x} \Ket{0 \oplus f(x)} - \frac {1}{\sqrt 2} \Ket{x} \Ket{1 \oplus f(x)} = $$ <!-- .element: class="fragment" -->

$$\frac {1}{\sqrt 2} (-1)^{f(x)} \Ket{x} (\Ket{0} - \Ket{1})$$ <!-- .element: class="fragment" -->

Заметим, что у нас не изменился вектор $y$, вместо этого просто вектор $x$ домножился на $(-1)$ в зависимости от того, какое значение принимает функция $f(x)$. <!-- .element: class="fragment" -->

Чтобы избежать склеивания мы можем использовать оператор Адамара. <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

$U_f$ при разных $f(x)$. 

Если $f(x) = 0$: <!-- .element: class="fragment" -->

$$U_f \Ket{x} \Ket{y} = \Ket{x} | y \oplus 0 > = \Ket{x} \Ket{y}$$ <!-- .element: class="fragment" -->

То есть ничего не происходит. <!-- .element: class="fragment" -->

<img src="/images/tcm_06_05_2021/d1.svg" /> <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Если $f(x) = 1$:

$$U_f \Ket{x} \Ket{y} = \Ket{x} | y \oplus 1 > = \Ket{x} X\Ket{y}$$ <!-- .element: class="fragment" -->

$y$ инвертируется. <!-- .element: class="fragment" -->

<img src="/images/tcm_06_05_2021/d2.svg" /> <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Если $f(x) = x$:

$$U_f \Ket{x} \Ket{y} = \Ket{x} \Ket{y \oplus x}$$ <!-- .element: class="fragment" -->

Это аналогично применению гейта $CNOT$. <!-- .element: class="fragment" -->


<img src="/images/tcm_06_05_2021/d3.svg" /> <!-- .element: class="fragment" -->


---

### Алгоритм Дойча

Если $f(x) = \bar{x}$:

$$U_f \Ket{x} \Ket{y} = \Ket{x} \Ket{y \oplus \bar{x}}$$ <!-- .element: class="fragment" -->

Это аналогично применению гейта $CNOT$ относительно инвертированного $x$. <!-- .element: class="fragment" -->
 
 
<img src="/images/tcm_06_05_2021/d4.svg" /> <!-- .element: class="fragment" -->

---

### Алгоритм Дойча

Таким образом, мы построили квантовый оракул для нашего алгоритма.

Сам алгоритм будет иметь вид: <!-- .element: class="fragment" -->

<img src="/images/tcm_06_05_2021/d_final.svg" /> <!-- .element: class="fragment" -->


---

### Алгоритм Дойча

Покажем, что нужно вычислить $f(x)$ всего лишь один раз:

$$\Ket{01} \rightarrow^{H_2} \frac{1}{2}(\Ket{0} + \Ket{1})(\Ket{0} - \Ket{1}) = \frac{1}{2} \Ket{0} (\Ket{0} - \Ket{1}) + \frac{1}{2} \Ket{1} (\Ket{0} - \Ket{1}) \rightarrow^{U_f}$$ <!-- .element: class="fragment" -->
$$\frac{1}{2} (-1)^{f(0)}\Ket{0} (\Ket{0} - \Ket{1}) +  \frac{1}{2} (-1)^{f(1)} \Ket{1} (\Ket{0} - \Ket{1}) =$$ <!-- .element: class="fragment" -->
$$\frac{1}{2} ((-1)^{f(0)} \Ket{0} + (-1)^{f(1)} \Ket{1} )(\Ket{0} - \Ket{1}) = $$
$$\frac{1}{\sqrt 2}((-1)^{f(0)} \Ket{0} +  (-1)^{f(1)} \Ket{1} ) \frac{1}{\sqrt 2} (\Ket{0} - \Ket{1}) = \Ket{q_1}\Ket{q_2}$$ <!-- .element: class="fragment" -->

если $f(x) = const$: <!-- .element: class="fragment" -->

$\pm \frac {1}{\sqrt 2} (\Ket{0} + \Ket{1}) = \pm \Ket{+} \rightarrow^{H} \Ket{0} \rightarrow^{M} 0$ <!-- .element: class="fragment" -->

если $f(x) \neq const$: <!-- .element: class="fragment" -->

$\pm \frac {1}{\sqrt 2} (\Ket{0} - \Ket{1}) = \pm \Ket{-} \rightarrow^{H} \Ket{1} \rightarrow ^{M} 1$ <!-- .element: class="fragment" -->

$U_f$ - применяется ровно 1 раз, следовательно, один вызов $f(x)$.  <!-- .element: class="fragment" -->
 
Таким образом, мы получили ускорение в 2 раза на квантовом компьютере. <!-- .element: class="fragment" -->

---

### Язык Q#

Q# (Q Sharp) — предметно-ориентированный язык, используемый для выражения квантовых алгоритмов. 

Базовая возможность языка: создание и использование кубитов для алгоритмов. Как следствие — одна из наиболее характерных особенностей Q# — возможность запутываться и создавать квантовую суперпозицию между кубитами через вентили CNOT и Адамара (H), соответственно. <!-- .element: class="fragment" -->

---

### Суперпозиции на Q#

Пусть у нас есть $N$ кубитов в нулевых состояниях $\Ket{0\dots0}$. Требуется применить к ним операторы, которые позволят получить суперпозицию базисных векторов в системе из $N$ кубитов. 

$$\Ket{S} = \frac{1}{\sqrt{2^N}}(\Ket{0\dots0} + \dots + \Ket{1\dots1})$$ <!-- .element: class="fragment" -->

---

### Суперпозиции на Q#

Как мы решили ранее, для этого надо ко всем входным кубитам применить оператор Адамара.

```
namespace Solution {
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;

    operation Solve (qs : Qubit[]) : ()
    {
        body
        {
            for (i in 1 .. Length(qs)) {
            H(qs[i-1]);
        }
    }
}
```

---

### Суперпозиции на Q#

Пусть теперь нам надо сгенерировать суперпозицию для двух базисных состояний $\psi$ и $\phi$ длины $N$.

$$\Ket{S} = \frac{1}{\sqrt 2}(\Ket{\psi} + \Ket{\phi})$$ <!-- .element: class="fragment" -->

---

### Суперпозиции на Q#

Здесь у нас предполагается, что базовые состояния задаются заранее и нам надо просто сделать программу, которая сгенерирует состояние, соответствующее запутанному между ними.

Алгоритм у нас ранее был разработан такой ($\delta$ -- это номер первого различающегося кубита): 
1. Если $\psi_i = \phi_i = 0$, то не делаем ничего. <!-- .element: class="fragment" -->
2. Если $\psi_i = \phi_i = 1$, то применяем гейт $X$, который изменит кубит на противоположный. <!-- .element: class="fragment" -->
1. Если $\psi_i = \psi_\delta, \phi_i = \phi_\delta$, то спутываем нулевые кубиты. <!-- .element: class="fragment" -->
2. Если $\psi_i \ne \psi_\delta, \psi_i \ne \psi_\delta$, то спутываем нулевые кубиты и инвертируем полученный кубит. <!-- .element: class="fragment" -->

---

### Суперпозиции на Q#

```

function FindFirstDiff (bits0 : Bool[], bits1 : Bool[]) : Int   
{
    mutable firstDiff = -1;
    for (i in 0 .. Length(bits1)-1) {
        if (bits1[i] != bits0[i] && firstDiff == -1) {
            set firstDiff = i;
        }
    }
    return firstDiff;
}

```

---

### Суперпозиции на Q#

```

namespace Solution {
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;


operation Solve (qs : Qubit[], bits0 : Bool[], bits1 : Bool[]) : ()
    {
        body
        {
            let firstDiff = FindFirstDiff(bits0, bits1)

            H(qs[firstDiff]);
            for (i in 0 .. Length(qs)-1) {
                if (bits0[i] == bits1[i]) {
                    if (bits0[i]) {
                        X(qs[i]);
                    }
                }
                else {
                    if (i > firstDiff) {
                        CNOT(qs[firstDiff], qs[i]);
                        if (bits0[i] != bits0[firstDiff]) {
                            X(qs[i]);
                        }
                    }
                } 
            }
        }
    }
}

```


---

### Различение состояний на Q#

Пусть система может находится только в одном из двух состояний: 

$$\Ket{0\dots0}$$
$$\Ket{W} = \frac{1}{\sqrt N}(\Ket{10\dots0} + \Ket{01\dots0} + \Ket{00\dots1})$$ <!-- .element: class="fragment" -->

---

### Различение состояний на Q#

```
namespace Solution {
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon

    operation Solve (qs : Qubit[]) : Int
    {
        body
        {
            mutable countOnes = 0;
            for (i in 0..Length(qs)-1) {
            if (M(qs[i]) == One) {
                    set countOnes = countOnes + 1;
                }
            }
            if (countOnes == 0) {
                return 0;
            }
            return 1;
        }
    }
}
```

---

### Различение состояний на Q#

Пусть требуется различить состояния $\Ket{0}$ и $\Ket{+}$ с вероятностью 80%. При этом известно, что система может находится в одном из этих двух состояний с вероятностью 50%.

Мы пришли к выводу, что надо сделать поворот на $\frac{\pi}{8}$ радиан.  <!-- .element: class="fragment" -->

Оператор поворота на угол $\alpha$: $R_y(2\alpha)$. <!-- .element: class="fragment" -->

---

### Различение состояний на Q#

```
namespace Solution {
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Extensions.Convert;
    open Microsoft.Quantum.Extensions.Math;

    operation Solve (q : Qubit) : Int
    {
        body
        {
            Ry(0.25*PI(), q);
            if (M(q) == Zero) {
                return 0;
            }
            return 1;
        }
    }
}
```

---

### Квантовые оракулы на Q#

Для заданного вектора {0,1} $\pmb{b}$ и входящего вектора кубитов $\pmb{x}$ реализовать функцию, $f(\pmb{x}) = (\pmb{b}\pmb{x} + (\pmb{1}- \pmb{b})(\pmb{1} - \pmb{x})) \mod 2$. 

Выход функции записать в кубит $\pmb{y}$. Количество кубитов $N$. <!-- .element: class="fragment" -->

---

### Квантовые оракулы на Q#

```
namespace Solution {
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;

    operation Solve (x : Qubit[], y : Qubit, b : Int[]) : ()
    {
        body
        {
            for (i in 0..Length(x)-1) {
            if (b[i] == 1) {
                CNOT(x[i], y);
            } else {
                X(x[i]);
                CNOT(x[i], y);
                X(x[i]);
                }
            }
        }
    }
}
```

---

### Алгоритм Дойча на Q#

Для данного алгоритма нам надо написать все вариации оракула.


```
operation OracleF0(q1 : Qubit, q2 : Qubit) : Unit is Adj {
    // constant 0
    // f(0) = f(1) = 0
}

operation OracleF1(q1 : Qubit, q2 : Qubit) : Unit is Adj  {
    // constant 1
    // f(0) = f(1) = 1
    X(q2);
}

operation OracleF2(q1 : Qubit, q2 : Qubit) : Unit is Adj  {
    // balanced same
    // f(0) = 0, f(1) = 1
    CNOT(q1, q2);
}

operation OracleF3(q1 : Qubit, q2 : Qubit) : Unit is Adj {
    // balanced opposite
    // f(0) = 1, f(1) = 0
    CNOT(q1, q2);
    X(q2);
}
```

---

### Алгоритм Дойча на Q#

Сам алгоритм.

```
operation RunDeutschAlogirthm(oracle : ((Qubit, Qubit) => Unit)) : Bool {
    mutable isFunctionConstant = true;
    using ((q1, q2) = (Qubit(), Qubit())) {
        X(q2);  
        H(q1);                                    
        H(q2);

        oracle(q1, q2);                       

        H(q1);                                     

        set isFunctionConstant = MResetZ(q1) == Zero;         
        Reset(q2);       
    }
    return isFunctionConstant;
}
```

---

### Алгоритм Дойча на Q#

Запуск программы.

```
@EntryPoint()
operation Main() : Unit {
    Message($"f0 is {RunDeutschAlogirthm(OracleF0) ? "constant" | "balanced"}.");
    Message($"f1 is {RunDeutschAlogirthm(OracleF1) ? "constant" | "balanced"}.");
    Message($"f2 is {RunDeutschAlogirthm(OracleF2) ? "constant" | "balanced"}.");
    Message($"f3 is {RunDeutschAlogirthm(OracleF3) ? "constant" | "balanced"}.");
}
```

---

### Понятие сложности алгоритмов

Вычисли́тельная сло́жность — понятие в информатике и теории алгоритмов, обозначающее функцию зависимости объёма работы, которая выполняется некоторым алгоритмом, от размера входных данных. 

Раздел, изучающий вычислительную сложность, называется теорией сложности вычислений. <!-- .element: class="fragment" -->

---

### Понятие сложности алгоритмов

Временная сложность алгоритма (в худшем случае) — это функция от размера входных данных, равная максимальному количеству элементарных операций, проделываемых алгоритмом для решения экземпляра задачи указанного размера.

По аналогии с временной сложностью, определяют пространственную сложность алгоритма, только здесь говорят не о количестве элементарных операций, а об объёме используемой памяти. <!-- .element: class="fragment" -->


---

### Понятие сложности алгоритмов

Обычно определяют сложность не точно, а смотрят на её порядок.

|Обозначение|Интуитивное объяснение|Определение|
|-----------|----------------------|-----------|
|$f(n) \in O(g(n))$|$f$ ограничена сверху функцией $g$ (с точностью до постоянного множителя) асимптотически|`$\exists (C>0), n_0 : \forall(n>n_0) \; |f(n)| \leq |Cg(n)|$ `или `$\exists (C>0), n_0 : \forall(n>n_0) \; f(n) \leq Cg(n)$||$f(n) \in \Omega(g(n))$`|
|$f(n) \in \Omega(g(n))$|$f$ ограничена снизу функцией $g$ (с точностью до постоянного множителя) асимптотически|`$\exists (C>0), n_0 : \forall (n>n_0) \; |Cg(n)| \leq |f(n)|$`|


---

### Понятие сложности алгоритмов


|Обозначение|Интуитивное объяснение|Определение|
|-----------|----------------------|-----------|
|$f(n) \in \Theta(g(n))$|$f$ ограничена снизу и сверху функцией $g$ асимптотически|`$\exists (C,C'>0), n_0 : \forall (n>n_0) \; |Cg(n)| \leq |f(n)| \leq |C'g(n)| $`|
|$f(n) \in o(g(n))$|$g$ доминирует над $f$ асимптотически|`$\forall (C>0),\exists n_0 : \forall(n>n_0) \; |f(n)| < |Cg(n)|$`|
|$f(n) \in \omega(g(n))$|$f$ доминирует над $g$ асимптотически |`$\forall (C>0),\exists n_0 : \forall(n>n_0) \; |Cg(n)| < |f(n)|$`|
|$f(n) \sim g(n)$|$f$ эквивалентна $g$ асимптотически|`$\lim_{n \to \infty} \frac{f(n)}{g(n)} = 1$`


---

### Расчёт сложности алгоритмов

Пусть $T_1(n)$ и $T_2(n)$ – время выполнения двух программных фрагментов $P_1$ и $P_2$. 

$T_1(n)$ имеет степень роста $O(f(n))$, а $T_2(n)$ – $O(g(n))$.  <!-- .element: class="fragment" -->

Тогда $T_1(n) + T_2(n)$, т. е. время последовательного выполнения фрагментов $P_1$ и $P_2$, имеет степень роста $O( \operatorname{max} (f(n), g(n)))$.  <!-- .element: class="fragment" -->

---

### Расчёт сложности алгоритмов

Правило сумм используется для вычисления времени последовательного выполнения программных фрагментов с циклами и ветвлениями. 

Предположим, что есть три фрагмента с временами выполнения соответственно $O(n^2)$, $O(n^3)$ и $O(n \log n)$.  <!-- .element: class="fragment" -->

Тогда время последовательного выполнения первых двух фрагментов име­ет порядок $O(max(n^2, n^3))$, т. е. $O(n^3)$. <!-- .element: class="fragment" -->

Время выполнения всех трех фрагментов имеет порядок $O(max(n^3, n \log n))$, это то же самое, что $O(n^3)$. <!-- .element: class="fragment" -->


---

### Расчёт сложности алгоритмов

Правило произведений заключается в следующем. Если $T_1(n)$ и $T_2(n)$  имеют сте­пени роста $O(f(n))$ и $O(g(n))$ соответственно, то произведение $T_1(n) \times T_2(n)$ имеет степень роста $O(f(n) \times g(n))$.  

Из пра­вила произведений следует, что $O(cf(n))$ эквивалентно $O(f(n))$, если $с$ – положитель­ная константа. Например, $O(\frac{n^2}{2})$ эквивалентно $O(n^2)$. <!-- .element: class="fragment" -->

---

### Расчёт сложности алгоритмов

Для примера возьмём Heap-sort. Это сортировка кучей.

Необходимо отсортировать массив $A$, размером $n$.  <!-- .element: class="fragment" -->

Построим на базе этого массива за $O(n)$ кучу для максимума.  <!-- .element: class="fragment" -->

Так как максимальный элемент находится в корне, то если поменять его местами с $A_{n−1}$, он встанет на своё место.  <!-- .element: class="fragment" -->

Далее за $O(\log n)$ просеет $A_{0}$ на нужное место и сформирует новую кучу (так как мы уменьшили её размер, то куча располагается с $A_0$ по $A_{n−2}$, а элемент $A_{n−1}$ находится на своём месте).  <!-- .element: class="fragment" -->

---

### Расчёт сложности алгоритмов

Повторим эту процедуру для новой кучи, только корень будет менять местами не с $A_{n−1}, а с $A_{n−2}$. 

Делая аналогичные действия, пока размер кучи не станет равен 1, мы будем ставить наибольшее из оставшихся чисел в конец не отсортированной части.  <!-- .element: class="fragment" -->

Очевидно, что таким образом, мы получим отсортированный массив. <!-- .element: class="fragment" -->

---

### Расчёт сложности алгоритмов

Время работы алгоритма складывается из:

1. $O(n)$ - построение кучи <!-- .element: class="fragment" -->
2. $O(\log n)$ - перестроение кучи, при снятии верхнего элемента <!-- .element: class="fragment" -->
3. Нам надо перестроить кучу $n$ раз. <!-- .element: class="fragment" -->

Итого: $O(n) + O(n \log n) = O(n \log n)$ по времени. <!-- .element: class="fragment" -->

$O(1)$ по памяти, так как нет дополнительных расходов памяти. <!-- .element: class="fragment" -->

---

### Классы сложности

Классом $DTIME(f(n))$ называется множество языков, для которых существует машина Тьюринга такая, что она всегда останавливается, и время ее работы не превосходит $f(n)$, где $n$ — длина входа. 

Классом $NTIME(f)$ по аналогии с $DTIME$ называется класс языков(задач), для которых существует недетерминированная машина Тьюринга, такая, что она всегда останавливается, и время ее работы не превосходит $f(n)$, где $n$ - длина входа. <!-- .element: class="fragment" -->

---

### Классы сложности

Класс $P$ — класс языков (задач), разрешимых на детерминированной машине Тьюринга за полиномиальное время, то есть: $P=\bigcup\limits_{p \in poly}DTIME(p(n))$.

Итого, язык $L$ лежит в классе $P$ тогда и только тогда, когда существует такая детерминированная машина Тьюринга $M$, что: <!-- .element: class="fragment" -->

* $M$ завершает свою работу за полиномиальное время на любых входных данных; <!-- .element: class="fragment" -->
* если на вход машине m подать слово $l∈L$, то она допустит его; <!-- .element: class="fragment" -->
* если на вход машине m подать слово $l∉L$, то она не допустит его. <!-- .element: class="fragment" -->

---

### Классы сложности

Класс задач, разрешимых за полиномиальное время достаточно широк, вот несколько его представителей:

* определение связности графов; <!-- .element: class="fragment" -->
* вычисление наибольшего общего делителя; <!-- .element: class="fragment" -->м
* задача линейного программирования; <!-- .element: class="fragment" -->
* проверка простоты числа. <!-- .element: class="fragment" -->

---

### Классы сложности

Класс регулярных языков входит в класс $P$, то есть: $Reg ⊂ P$.

Класс контекстно-свободных языков входит в класс $P$, то есть: $CFL⊂P$. <!-- .element: class="fragment" -->

---

### Классы сложности

В теории сложности Класс $NP$ — класс языков (задач), ответ на которые можно проверить за полиномиальное время.

Формальное определение класса $NP$ через класс $NTIME$ выглядит так: <!-- .element: class="fragment" -->

$$NP = \bigcup_{i=0}^{\infty}NTIME(in^i)=\bigcup_{i=0}^{\infty}\bigcup_{k=0}^{\infty}NTIME(in^k)$$ <!-- .element: class="fragment" -->

Для этого класса определено понятие сертификата (решения) -- оно должно проверяться полиномиальным алгоритмом. <!-- .element: class="fragment" -->

---

### Классы сложности

Можно привести много задач, про которые на сегодняшний день неизвестно, принадлежат ли они P, но известно, что они принадлежат NP. Среди них:

* Задача выполнимости булевых формул: узнать по данной булевой формуле, существует ли набор входящих в неё переменных, обращающий её в 1. Сертификат — такой набор. <!-- .element: class="fragment" -->
* Задача о клике: по данному графу узнать, есть ли в нём клики (полные подграфы) заданного размера. Сертификат — номера вершин, образующих клику. <!-- .element: class="fragment" -->
* Определение наличия в графе гамильтонова цикла. Сертификат — последовательность вершин, образующих гамильтонов цикл.
* Неоптимизационный вариант задачи о коммивояжёре (существует ли маршрут не длиннее, чем заданное значение k) — расширенный и более приближенный к реальности вариант предыдущей задачи. Сертификат - такой маршрут. <!-- .element: class="fragment" -->
* Существование целочисленного решения у заданной системы линейных неравенств. Сертификат — решение. <!-- .element: class="fragment" -->

---

### Классы сложности

$NP$-полная задача — в теории алгоритмов задача с ответом «да» или «нет» из класса $NP$, к которой можно свести любую другую задачу из этого класса за полиноминальное время (то есть при помощи операций, число которых не превышает некоторого полинома в зависимости от размера исходных данных).  

Таким образом, $NP$-полные задачи образуют в некотором смысле подмножество «типовых» задач в классе $NP$: если для какой-то из них найден «полиномиально быстрый» алгоритм решения, то и любая другая задача из класса $NP$ может быть решена так же «быстро». <!-- .element: class="fragment" -->

Все остальные задачи класса $NP$ называются $NP$-трудними. <!-- .element: class="fragment" -->

---

### Классы сложности

Проблема равенства $P = NP$ состоит в следующем: если положительный ответ на какой-то вопрос можно довольно быстро проверить (за полиномиальное время), то правда ли, что ответ на этот вопрос можно довольно быстро найти (также за полиномиальное время и используя полиномиальную память)?  

Другими словами, действительно ли решение задачи проверить не легче, чем его отыскать? <!-- .element: class="fragment" -->

---

### Классы сложности

<img src="/images/tcm_17_05_2021/P_np_np-complete_np-hard.svg" />

---

### Классы сложности

На самом деле классов сложности более 400 сейчас.

https://complexityzoo.net/Complexity_Zoo 

Поэтому тут не будет той картинки из вики, с некоторыми классами сложности ^_^ <!-- .element: class="fragment" -->

---

### Заключение

На этом курс лекций подошёл к концу.

Часть материала по квантовым вычислениям и сложностям будет выложена для самостоятельного изучения на следующей неделе.

Спасибо!
