---
customTheme : "presentation"

---

## Лекция 3. Структуры данных, отложенные вызовы, обработка ошибок и основы тестирования

### OZON

### Москва, 2021


---

### Лекции

1. <span style="color:gray">Введение. Рабочее окружение. Структура программы. Инструментарий.</span>
2. <span style="color:gray">Базовые конструкции и операторы. Встроенные типы и структуры данных.</span>
3. Структуры данных, отложенные вызовы, обработка ошибок и основы тестирования
4. <span style="color:gray">Интерфейсы, моки и тестирование с ними</span>
5. <span style="color:gray">Асинхронные сущности и паттерны в Go</span>
6. <span style="color:gray">Protobuf и gRPC</span>
7. <span style="color:gray">Работа с БД в Go</span>
8. <span style="color:gray">Брокеры сообщений. Трассировка. Метрики. </span>

---

### Темы

Сегодня мы поговорим про:

1. ZeroValue <!-- .element: class="fragment" -->
1. Указатели <!-- .element: class="fragment" -->
1. Встроенные коллекции <!-- .element: class="fragment" -->
1. Множества <!-- .element: class="fragment" -->
1. Строки <!-- .element: class="fragment" -->
1. Отложенные вызовы <!-- .element: class="fragment" -->
1. Error <!-- .element: class="fragment" -->
1. Пользовательские структуры <!-- .element: class="fragment" -->
1. Тестирование <!-- .element: class="fragment" -->

---

### Чему мы узнаем сегодня?

1. Как работать указателями. <!-- .element: class="fragment" -->
1. Какие есть встроенные коллекции и когда их можно и нужно применять <!-- .element: class="fragment" -->
1. Как работать со строками<!-- .element: class="fragment" -->
1. Как сделать так, чтобы при любом выходе из функции выполнился определённый код <!-- .element: class="fragment" -->
1. Как в GoLang организована работа с ошибками <!-- .element: class="fragment" -->
1. Как создать свою структуру, связать с ней функции  <!-- .element: class="fragment" -->
1. Как писать тесты!  <!-- .element: class="fragment" -->

---

### Обозначения

* 📽️ - посмотри воркшоу
* ⚗️ - проведи эксперимент
* 🔬 - изучи внимательно
* 📖 - прочитай документация
* 🪙 - подумай о сложности
* 🐞 - запомни ошибку
* 🔨 - запомни решение
* 🏔️ - обойди камень предкновенья
* ⏰ - сделай перерыв
* 🏡 - попробуй дома
* 💡 - обсуди светлые идеи
* 🙋 - задай вопрос
* ⚡ - запомни панику

---

### ZeroValue

Значения, которые по-умолчанию задаются при объявлении переменных заданного типа.

| Типы        | Значения |
| ----------- | -------- |
| `string`    | ""       |
| `map`       | nil      |
| `slice`     | nil      |
| `bool`      | false    |
| **Pointer** | nil      |
| **Numeric** | 0        |

---

### Указатели

Инициализация: **var**, **new**, **make**

Размер: 32, 64

Операции: разыменование и получение адреса

Специальный тип: **uintptr**

---

### Указатели

```go
package main

import (
  "fmt"
  "unsafe"
)

func main() {
  var result *uint     // zero-value
  answer := new(int64) // new or make
  
  fmt.Printf("size: %d", unsafe.Sizeof(result))
  
  if answer != nil {
    fmt.Printf("answer: %d", *answer)
  }

  fmt.Printf("answer: %d", *result) // ⚡
  
  answer := new(int64) // new or make
  *answer = 42

  foo := 0
  result = &foo
}
```

---

### Коллекции. List (двусвязный список)

```
type Element
  func (e *Element) Next() *Element
  func (e *Element) Prev() *Element
type List
  func New() *List
  func (l *List) Back() *Element
  func (l *List) Front() *Element
  func (l *List) Init() *List
  func (l *List) InsertAfter(v interface{}, mark *Element) *Element
  func (l *List) InsertBefore(v interface{}, mark *Element) *Element
  func (l *List) Len() int
  func (l *List) MoveAfter(e, mark *Element)
  func (l *List) MoveBefore(e, mark *Element)
  func (l *List) MoveToBack(e *Element)
  func (l *List) MoveToFront(e *Element)
  func (l *List) PushBack(v interface{}) *Element
  func (l *List) PushBackList(other *List)
  func (l *List) PushFront(v interface{}) *Element
  func (l *List) PushFrontList(other *List)
  func (l *List) Remove(e *Element) interface{}
```

---

### Коллекции. Heap (Куче)

Используется для очередей с приоритетами. Требуется, чтобы элемент кучи реализовывал интерфейс.

```
  func Fix(h Interface, i int)
  func Init(h Interface)
  func Pop(h Interface) interface{}
  func Push(h Interface, x interface{})
  func Remove(h Interface, i int) interface{}
```

---

### Коллекции. Куча минимума.

```
package main

import (
	"container/heap"
	"fmt"
)

// An IntHeap is a min-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

// This example inserts several ints into an IntHeap, checks the minimum,
// and removes them in order of priority.
func main() {
	h := &IntHeap{2, 1, 5}
	heap.Init(h)
	heap.Push(h, 3)
	fmt.Printf("minimum: %d\n", (*h)[0])
	for h.Len() > 0 {
		fmt.Printf("%d ", heap.Pop(h))
	}
}
```

---

### Коллекции. Ring (кольцевой список)

```
type Ring
  func New(n int) *Ring
  func (r *Ring) Do(f func(interface{}))
  func (r *Ring) Len() int
  func (r *Ring) Link(s *Ring) *Ring
  func (r *Ring) Move(n int) *Ring
  func (r *Ring) Next() *Ring
  func (r *Ring) Prev() *Ring
  func (r *Ring) Unlink(n int) *Ring

```

---

### Множества


В go нет своего типа для множеств, так как нет дженериков (пока). Но мы можем сделать его сами.

Как лучше? 

```go
map[uint]bool

map[uint]struct{}
```

> TL;DR map[]struct{} is 5% faster in time and 10% less memory consumption comparing to map[]bool when it comes to a big Set.  <!-- .element: class="fragment" -->

---

### Множества

Инициализация

```go
var userIds map[uint]struct{} // nil - zero value

var userIds map[uint]struct{}{}

userIds := make(map[uint]struct{})

userIds := make(map[uint]struct{}, len(users)) // with capacity

println(userIds)

```

---

### Множества

Инициализация

```go
userIds := map[uint]struct{}{1, 2, 3} // compilation error 🐞

userIds = map[uint]struct{}{ // 🔨
  1: {},
  2: {},
  3: {}, // struct{}
}
```

Чтение

```go
userIds := map[uint]struct{}{1: {}}
println(userIds[1])
println(userIds[2]) // zero-value
```

Сравнение двух множеств 🏡 💡

---

### Множества

- Итерирование

```go
for userId, _ := range userIds { // 🐞 semantic error
}

for userId := range userIds {
}
```

- Удаление

```go
if _, found := userIds[1]; found { // 🙋 semantic error ?
  delete(userIds, 1)
}

delete(userIds, 1)

---

### Множества

- Копирование

```go
src := map[uint]struct{}{1: {}}

dst := src
// dst[2] = struct{}{}
// println(src)
```

```go
src := map[uint]struct{}{1: {}}

dst := make(map[uint]struct{}, len(src))

for key := range src {
  dst[key] = struct{}{}
}
// dst[2] = struct{}{}
// println(src)
```

---

### Строки

> In Go, a **string** is in effect a read-only <u>slice</u> of bytes

> Any slice in Go stores the length (in bytes), so you don't have to care about the cost of the `len` operation : there is no need to count 🪙

> Go strings aren't **null** terminated, so you don't have to remove a null byte, and you don't have to add `1` after slicing by adding an empty string.


---

### Строки

Литералы

- symbol -- ''
- text -- ""
- multiline text -- ``

А что если так?

```go
const nihongo = "日本語"
fmt.Printf("len: %d\n", len(nihongo))
fmt.Printf("cap: %d\n", cap(nihongo)) // 🙋 скомпилируется ?
```

---

### Строки (как массив байт)

- Индексирование

```go
const text = "日本語"
fmt.Println(string(text[2])) // 🙋 что будет выведено на экран ?
```

- Конвертация

```go
b := []byte("ABC€")
fmt.Println(b) // 🙋 что будет выведено на экран ?

s := string([]byte{65, 66, 67, 226, 130, 172})
fmt.Println(s) // 🙋 что будет выведено на экран ?
```

---

### Строки (как массив рун)

- Индексирование

```go
const text = "hello"
fmt.Println(text[0])
```

```go
func replaceAtIndex(in string, r rune, i int) string {
    out := []rune(in)
    out[i] = r
    return string(out)
}
```

- Итерирование

```go
for _, runeValue := range nihongo {
	fmt.Println("%u", runeValue)
}

---

### Строки (как массив рун)

- Конкатенация

```go
"123" + "456"
```

- Сравнение

```go
// Compare is included only for symmetry with package bytes. 
// It is usually clearer and always faster to use the built-in  
// string comparison operators ==, <, >, and so on.
if str1 == str2 {
}
```

---

### Строки 

- Пакет **strings**

|      | Go                        | 
| ---- | ------------------------- |
| 1    | `Contains`/`ContainsAny`  |
| 2    | `Count`                   |
| 3    | `HasPrefix`/`HasSuffix`   |      
| 4    | `Index`/`LastIndex`       |
| 5    | `IndexAny`/`LastIndexAny` |      
| 6    | `Replace`/`ReplaceAll`    |      
| 7    | `Title`/`ToTitle`         |      

---

### Строки 

|      | Go                        | 
| ---- | ------------------------- |
| 8    | `ToLower`/`ToUpper`       |      
| 9    | `Trim`                    |      
| 10   | `TrimLeft`/`TrimRight`    |      
| 11   | `TrimPrefix`/`TrimSuffix` |      
| 12   | `TrimSpace`               |      
| 13   | `Join`/`Split`            |      
| 14   | `Repeat`                  |      
| ...  | ...                       |   

> The rule Title uses for word boundaries does not handle Unicode punctuation properly. 🐞

---

### Отложенные вызовы

> A `defer` statement postpones the execution of a function until the surrounding function returns, either normally <u>or</u> through a panic

```go
func main() {
  for i := 0; i < 5; i++ {
		defer fmt.Println(i) // 🙋 что будет выведено на экран ?
	}
	fmt.Println(5)
}
```

```go
for {
	file := openFile(filePath)
	defer closeFile(file) // 🐞 semantic error
}
fmt.Println(5)
```

---

### Отложенные вызовы

```go
func main() {
    i := 0
    defer fmt.Println(i) // ⚗️ проверь с другими типами
    i++
}
```

```go
func main() {
  defer fmt.Println("world")
  panic("stop")
  fmt.Println("hello")
}
```

```go
func getName() (result string) {
    defer func() { result = "Veronika" }()
    return "Vika"
}
```

```go
func getName() (result string) {
    defer func() { result = "Veronika" }()
    result = "Vika"
    return
}
```

---

### Восстановление работы после паники

```go
package main

import "fmt"

func recovering() {
  if obj:=recover(); obj != nil {
    fmt.Println(obj)
  }
  fmt.Println("recovered")
}

func executePanic() {
  defer recovering()
  panic("stop")
  fmt.Println("after panic")
}

func main() {
  executePanic()
  fmt.Println("end")
}

// stop
// recovered
// end
```

---

### Ошибки

- `error`

```go
err := errors.New("missed user") // return error
fmt.Println(err.Error())
```

```go
err = fmt.Errorf("... %w ...", ..., err, ...)
fmt.Errorf("cant describe user with id <%d> due: %w", userId, err)
```

```go
// import "errors", "os" and "io/fs"
if _, err := os.Open("non-existing"); err != nil {
  var pathError *fs.PathError
  if errors.As(err, &pathError) {
    fmt.Println("Failed at path:", pathError.Path)
  } else {
    fmt.Println(err)
  }
}
```

---

### Ошибки

```go
if errors.Is(err, fs.ErrExist) {
}
// vs
if err == fs.ErrExist { // 🐞 semantic error
}
```

```go
var pathError *fs.PathError
if errors.As(err, &pathError) {
	fmt.Println(perr.Path)
}
// vs
if pathError, ok := err.(*fs.PathError); ok { // 🐞 semantic error
	fmt.Println(pathError.Path)
}
```

---

### Ошибки


```go
type Error interface {
  Error() string
}
```

Наименование:

- **err**

- **specificError**
- **err1**, **err2**, ... 🏔️

---

### Ошибки

Error return design pattern:

```go
func foo(...) (result, err) {
 	// ...
}

func minmax(a,b uint) (min uint, max uint, error) {
  return 0, 0, errors.New("not implemented")
}

func minmax(a,b uint) (*MinMax, error) {
  return 0, 0, errors.New("not implemented")
}
```

- численный тип
- строка
- **array**
- **slice**
- **map**
- пользовательский тип
-  ...

Составной результат **vs** множества значений ? 🙋

---

### Структуры 

Создание пользовательского типа

```go
type uint8 TaskDifficulty

const (
  Beginner TaskDifficulty = iota
  Easy
  Normal
  Hard
)

```

---

### Структуры 

```
type Task struct {
  Description  string
  Difficulty   TaskDifficulty
}

func main() {
  
  task := Task{
    Description: "revert list",
    Difficulty: Beginner,
  }
  
  fmt.Println(task.Description)
}
```

---

### Структуры 

Объявления пользовательского типа:

- C перечислением полей на одной строке
- Поля как _ 💡
- Объявление внутри функции
- Анонимные структуры

---

### Структуры 

```go
type Vertex struct {
	X, Y float64
}

// value receiver
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// pointer receiver
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
	v.Scale(10)
	fmt.Println(v.Abs())
  var v2 Vertex
  fmt.Println(v2.Abs())
}
```

---

### Структуры 

> You can declare methods with pointer receiver.  This means the receiver type has the literal syntax `*T ` for some type `T`

> Also, `T` cannot itself be a pointer such as `*int`

---

### Тестирование

Тесты пишутся в файлы с суффиксом `_test.go`. Для запуска используется `go test`.

```go
// where Xxx does not start with a lowercase letter. 
// The function name serves to identify the test routine
func TestXxx(*testing.T)
​
func BenchmarkXxx(*testing.B)
​
func ExampleXxx()
```

```go
func TestAbs(t *testing.T) {
    got := Abs(-1)
    if got != 1 {
        t.Errorf("Abs(-1) = %d; want 1", got)
    }
}
```

---

### Тестирование

```
➜ go test [-s] [-v]
➜ go test -run ''      # Run all tests.
➜ go test -run Foo/A=  # For top-level tests matching "Foo", run subtests matching "A="
```

* `-v` -- Verbose output: log all tests as they are run. Also print all text from Log and Logf calls even if the test succeeds.
* `-cpu 1,2,4` -- Specify a list of GOMAXPROCS values for which the tests or benchmarks should be executed
* `-list regexp` -- List tests, benchmarks, or examples matching the regular expression.
* `-parallel` --  Allow parallel execution of test functions that call t.Parallel.
* `-run regexp` Run only those tests and examples matching the regular expression.
* `-short` -- Tell long-running tests to shorten their run time.

---

### Тестирование

* `t.Error*` will report test failures but continue executing the test. 

* `t.Fatal*` will report test failures and stop the test immediately.

---

### Тестирование

Общие методы для `testing.T` и `testing.B` представлены в виде интерфейса `TB`:

```
type TB interface {
    Cleanup(func())
    Error(args ...interface{})
    Errorf(format string, args ...interface{})
    Fail()
    FailNow()
    Failed() bool
    Fatal(args ...interface{})
    Fatalf(format string, args ...interface{})
    Helper()
    Log(args ...interface{})
    Logf(format string, args ...interface{})
    Name() string
    Skip(args ...interface{})
    SkipNow()
    Skipf(format string, args ...interface{})
    Skipped() bool
    TempDir() string
}
```

---

### Тестирование


Специализированные методы для `testing.T`:

* Deadline
* Run / Parallel

Специализированные методы для testing.B:

* StartTimer / StopTimer / ResetTimer
* SetBytes
* ReportMetric / ReportAllocs

---

### Тестирование

```
➜ go get github.com/stretchr/testify
```

```
import (
  "testing"
  "github.com/stretchr/testify/assert"
)
​
func TestSomething(t *testing.T) {
  // assert equality
  assert.Equal(t, 123, 123, "they should be equal")
  // assert inequality
  assert.NotEqual(t, 123, 456, "they should not be equal")
  // assert for nil (good for errors)
  assert.Nil(t, object)
  // assert for not nil (good when you expect something)
  if assert.NotNil(t, object) {
    // now we know that object isn't nil, we are safe to make
    // further assertions without causing any errors
    assert.Equal(t, "Something", object.Value)
  }
}
```

assert package:

* Prints friendly, easy to read failure descriptions
* Allows for very readable code
* Optionally annotate each assertion with a message

> The require package provides same global functions as the assert package, but instead of returning a boolean result they terminate current test.

---

### Полезные ссылки

1. [Set in Go, map[]bool and map[]struct{} performance comparison](https://itnext.io/set-in-go-map-bool-and-map-struct-performance-comparison-5315b4b107b)
