---
title: "Лекция 10. Упрощение грамматик. Древо разбора. Лемма о разрастании."
block: "Теоретические модели вычислений"
order: 10

---

## Теоретические модели вычислений. 
## Лекция 10. Упрощение грамматик. Древо разбора. Лемма о разрастании.

#### 29 марта 2021 года

---

### План занятия

1. Упрощение грамматик <!-- .element: class="fragment" -->
2. Деревья разбора <!-- .element: class="fragment" -->
3. Лемма о разрастании <!-- .element: class="fragment" -->

---

### Преобразования КС-грамматик

Для этого разберём доказательства вспомогательных алгоритмов:

1. Удаление бесполезных нетерминалов.<!-- .element: class="fragment" -->
1. Удаление $\lambda$ правил из грамматики.<!-- .element: class="fragment" -->
1. Удаление длинных правил из грамматики.<!-- .element: class="fragment" -->
1. Удаление цепных правил из грамматики.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил

Правила вида $A→\lambda$ называются $\lambda$-правилами.

Нетерминал $A$ называется $\lambda$-порождающим, если $A⇒^*\lambda$.<!-- .element: class="fragment" -->

---

### Поиск $\lambda$-порождающих терминалов

Пусть дана КС-грамматика $Γ=(N,Σ,S,P)$.

Требуется найти все $\lambda$-порождающие нетерминалы.

1. Находим все  $\lambda$-правила. Составить множество, состоящее из нетерминалов, входящих в левые части таких правил.<!-- .element: class="fragment" -->
2. Перебираем правила грамматики $Γ$. Если найдено правило $A→C_1C_2…C_k$, $k \in \mathbb{N}$, для которого верно, что каждый $C_i$ принадлежит множеству, то добавить $A$ в множество.<!-- .element: class="fragment" -->
3. Если на шаге 2 множество изменилось, то повторить шаг 2.<!-- .element: class="fragment" -->

---

### Поиск $\lambda$-порождающих терминалов

Теорема: Описанный выше алгоритм находит все $\lambda$-порождающие нетерминалы грамматики $Γ$. 

Док-во: для доказательства корректности алгоритма достаточно показать, что, если множество $\lambda$-порождающих нетерминалов на очередной итерации алгоритма не изменялось, то алгоритм нашел все $\lambda$-порождающие нетерминалы.<!-- .element: class="fragment" -->

---

### Поиск $\lambda$-порождающих терминалов

Пусть после завершения алгоритма существуют нетерминалы такие, что они являются $\lambda$-порождающими, но не были найдены алгоритмом. 

Выберем из этих нетерминалов нетерминал $B$, из которого выводится $\lambda$ за наименьшее число шагов. <!-- .element: class="fragment" -->

Тогда в грамматике есть правило $B→C_1C_2…C_k$, $k \in \mathbb{N}$, где каждый нетерминал $C_i$ — $\lambda$-порождающий.<!-- .element: class="fragment" -->

---

### Поиск $\lambda$-порождающих терминалов

Каждый $C_i$ входит в множество $\lambda$-порождающих нетерминалов, так как иначе вместо $B$ необходимо было взять $C_i$. 

Следовательно, на одной из итераций алгоритма $B$ уже добавился в множество $\lambda$-порождающих нетерминалов. <!-- .element: class="fragment" -->

Противоречие. <!-- .element: class="fragment" -->

Следовательно, алгоритм находит все $\lambda$-порождающие нетерминалы. Ч.т.д<!-- .element: class="fragment" -->

---

### Поиск $\lambda$-порождающих терминалов

Рассмотрим грамматику:

1. $S→ABC$
2. $S→DS$
3. $A→\lambda$
4. $B→AC$
5. $C→\lambda$
6. $D→d$

Возьмём множество состоящее из ε-порождающих нетерминалов $\{A,C\}$.<!-- .element: class="fragment" -->

Добавим $B$ в множество, так как правая часть правила $B→AC$ состоит только из нетерминалов из множества.<!-- .element: class="fragment" -->

Повторим второй пункт для правила $S→ABC$ и получим множество $\{A,B,C,S\}$.<!-- .element: class="fragment" -->

Больше нет нерассмотренных правил, содержащих справа только нетерминалы из множества.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Преобразуем КС-грамматику: $Γ=(N,Σ,S,P)$ к грамматике $Γ′=(N,Σ,S′,P′)$ без $\lambda$-правил.

Алгоритм:
1. Добавить все правила из $P$ в $P′$.<!-- .element: class="fragment" -->
2. Найти все $\lambda$-порождаюшие нетерминалы.<!-- .element: class="fragment" -->
3. Для каждого правила вида $A→α_0B_1α_1B_2α_2…B_kα_k$  (где $α_i$ — последовательности из терминалов и нетерминалов, $B_j$ — $\lambda$-порождающие нетерминалы) добавить в $P′$ все возможные варианты правил, в которых либо присутствует, либо удалён каждый из нетерминалов $B_j(1⩽j⩽k)$.<!-- .element: class="fragment" -->
4. Удалить все $\lambda$-правила из $P′$.<!-- .element: class="fragment" -->
5. Если в исходной грамматике $Γ$ выводилось $\lambda$, то необходимо добавить новый нетерминал $S′$, сделать его стартовым, добавить правило $S′→S|\lambda$.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Теорема: Если грамматика $Γ′$ была построена с помощью описанного выше алгоритма по грамматике $Γ$, то $L(Γ′)=L(Γ)$.

Док-во: Сначала докажем, что, если не выполнять шаг 5 алгоритма, то получится грамматика $Γ′:L(Γ′)=L(Γ)\setminus\{ \lambda \}$.<!-- .element: class="fragment" -->

Для этого достаточно доказать, что $(A\underset{\Gamma}{⇒}^* w) \iff (A\underset{\Gamma^\prime}{⇒}^*w \land w≠\lambda$).<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

$\Rightarrow$: пусть $A \underset{\Gamma'}{\Rightarrow}^*w$ и $w \ne \lambda$.<!-- .element: class="fragment" -->

Докажем индукцией по длине порождения в грамматике $\Gamma'$, что $A \underset{\Gamma}{\Rightarrow}^*w$.<!-- .element: class="fragment" -->

База: $A \underset{\Gamma'}{\Rightarrow} w$.<!-- .element: class="fragment" -->

В этом случае в $\Gamma'$ есть правило $A \rightarrow w$.<!-- .element: class="fragment" -->

По построению $\Gamma'$ в $\Gamma$ есть правило $A \rightarrow \alpha$, причем $\alpha$ — цепочка $w$, элементы которой, возможно, перемежаются $\lambda$-порождающими нетерминалами.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Тогда в $\Gamma$ есть порождения $A \underset{\Gamma}{\Rightarrow} \alpha \underset{\Gamma}{\Rightarrow}^*w$. 

Предположение индукции: пусть из `$A \underset{\Gamma^\prime}{\Rightarrow}^*w \ne \lambda$ ` менее, чем за $n$ шагов, следует, что $A \underset{\Gamma}{\Rightarrow}^*w$.<!-- .element: class="fragment" -->

Переход: пусть в порождении $n$ шагов, $n > 1$. Тогда оно имеет вид $A\underset{\Gamma'}{\Rightarrow}X_1 X_2 \ldots X_k \underset{\Gamma'}{\Rightarrow}^*w$, где $X_i \in N \cup \Sigma $.<!-- .element: class="fragment" -->

Первое использованное правило должно быть построено по правилу грамматики $\Gamma$ $A \rightarrow Y_1 Y_2 \ldots Y_m$, где последовательность $Y_1 Y_2 \ldots Y_m$ совпадает с последовательностью $X_1 X_2 \ldots X_k$, символы которой, возможно, перемежаются $\lambda$-порождающими нетерминалами.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Цепочку $w$ можно разбить на $w_1 w_2 \ldots w_k$, где $X_i    \underset{\Gamma'}{\Rightarrow}^*w_i$.  

Если $X_i$ — терминал, то $w_i = X_i$, a если нетерминал, то порождение $X_i \underset{\Gamma'}{\Rightarrow}^* w_i$ содержит менее $n$ шагов. <!-- .element: class="fragment" -->

По предположению $X_i \underset{\Gamma}{\Rightarrow}^* w_i$, значит $A \underset {\Gamma}{\Rightarrow} Y_1 Y_2 \ldots Y_m \underset{\Gamma}{\Rightarrow}^* X_1 X_2 \ldots X_k \underset{\Gamma}{\Rightarrow}^* w_1 w_2 \ldots w_k = w$.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

$\Leftarrow$: пусть $A \underset{\Gamma}{\Rightarrow}^*w$& и $w \ne \lambda$. 

Докажем индукцией по длине порождения в грамматике $\Gamma$, что $A \underset{\Gamma'}{\Rightarrow}^*w$. <!-- .element: class="fragment" -->

База: $A \underset{\Gamma}{\Rightarrow} w$.
Правило $A \rightarrow w$ присутствует в $\Gamma$. Поскольку $w \ne \lambda$, это же правило будет и в $\Gamma'$, поэтому $A \underset{\Gamma'}{\Rightarrow}^*w$.<!-- .element: class="fragment" -->

Предположение индукции: Пусть из $A \underset{\Gamma}{\Rightarrow} ^\ast w \ne \lambda$ менее, чем за $n$ шагов, следует, что $A \underset{\Gamma'}{\Rightarrow}^*w $.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Переход: пусть в порождении $n$ шагов, $n > 1$. Тогда оно имеет вид $A\underset{\Gamma}{\Rightarrow}Y_1 Y_2 \ldots Y_m \underset{\Gamma}{\Rightarrow}^*w$, где $Y_i \in N \cup \Sigma $. 

Цепочку $w$ можно разбить на $w_1 w_2 \ldots w_m$, где $Y_i \underset{\Gamma}{\Rightarrow}^*w_i$. <!-- .element: class="fragment" -->

Пусть $Y_{i_1}, Y_{i_2}, \ldots, Y_{i_p}$ — подпоследовательность, состоящая из всех элементов, таких, что $w_{i_k} \ne \lambda$, то есть $Y_{i_1} Y_{i_2} \ldots Y_{i_p} \underset{\Gamma}{\Rightarrow}^*w$. $p \geqslant 1$, поскольку $w \ne \lambda$. <!-- .element: class="fragment" -->

Значит, $A \rightarrow Y_{i_1} Y_{i_2} \ldots Y_{i_p}$ является правилом в $\Gamma'$ по построению $\Gamma'$. <!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 


Так как каждое из порождений $Y_i \underset{\Gamma}{\Rightarrow}^* w_i$ содержит менее $n$ шагов, к ним можно применить предположение индукции и заключить, что, если $w_i \ne \lambda$, то $Y_i \underset{\Gamma'}{\Rightarrow}^* w_i$.

Таким образом, $A \underset{\Gamma'}{\Rightarrow} Y_{i_1} Y_{i_2} \ldots Y_{i_p} \underset{\Gamma'}{\Rightarrow}^* w$.<!-- .element: class="fragment" -->

Подставив $S$ вместо $A$ в утверждение (*), видим, что $w \in L(\Gamma)$ для $w \ne \lambda$ тогда и только тогда, когда $w \in L(\Gamma')$.<!-- .element: class="fragment" -->

Так как после выполнения шага 5 алгоритма в $\Gamma'$ могло добавиться только пустое слово $\lambda$, то язык, задаваемый КС-грамматикой $\Gamma'$, совпадает с языком, задаваемым КС-грамматикой $\Gamma$.<!-- .element: class="fragment" -->

Ч.т.д.<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Рассмотрим грамматику:
* $S\rightarrow ABCd$
* $A\rightarrow a|\lambda$
* $B\rightarrow AC$
* $C\rightarrow c|\lambda$

В ней $A$, $B$ и $C$ являются $\lambda$-порождающими нетерминалами.<!-- .element: class="fragment" -->

Переберём для каждого правила все возможные сочетания $\lambda$-порождающих нетерминалов и добавим новые правила:<!-- .element: class="fragment" -->
* $S\rightarrow Ad|ABd|ACd|Bd|BCd|Cd|d$ для $S \rightarrow ABCd$<!-- .element: class="fragment" -->
* $B \rightarrow A|C$ для $B \rightarrow AC$<!-- .element: class="fragment" -->

---

### Удаление $\lambda$-правил 

Удалим праила $A\rightarrow \lambda$ и $C\rightarrow \lambda$<!-- .element: class="fragment" -->

В результате мы получим новую грамматику без $\lambda$-правил: 
* $S\rightarrow Ad|ABd|ACd|ABCd|Bd|BCd|Cd|d$<!-- .element: class="fragment" -->
* $A\rightarrow a$<!-- .element: class="fragment" -->
* $B\rightarrow A|AC|C$<!-- .element: class="fragment" -->
* $C\rightarrow c$<!-- .element: class="fragment" -->

---

### Удаление длинных правил из КС-грамматики

Пусть $Γ$ — контекстно-свободная грамматика. Правило $A→β$ называется длинным, если $|β|>2$.

С каждым длинным правилом $A→a_1a_2…a_k$, $k>2$, $a_i∈Σ∪N$ проделаем следующее:<!-- .element: class="fragment" -->

1. Добавим в грамматику $k−2$ новых нетерминала $B_1,B_2,…B_{k−2}$.<!-- .element: class="fragment" -->

2. Добавим в грамматику $k−1$ новое правило: <!-- .element: class="fragment" -->
    * $A→a_1B_1$<!-- .element: class="fragment" -->
    * $B_1→a_2B_2$<!-- .element: class="fragment" -->
    * $B_2→a_3B_3$<!-- .element: class="fragment" -->
    * $…$<!-- .element: class="fragment" -->
    * $B_{k−2}→a_{k−1}a_k$<!-- .element: class="fragment" -->

3. Удалим из грамматики правило $A→a_1a_2…a_k$.<!-- .element: class="fragment" -->

---

### Удаление длинных правил из КС-грамматики

Теорема: Пусть $\Gamma$ - КС-грамматика $\Gamma'$ - грамматика, полученная в результате применения алгоритма к $\Gamma$. Тогда $L(\Gamma) = L(\Gamma')$.

Док-во:<!-- .element: class="fragment" -->

$\Rightarrow $ <!-- .element: class="fragment" -->

Покажем, что $L(\Gamma) \subseteq L(\Gamma')$. <!-- .element: class="fragment" -->
 
Пусть $w \in L(\Gamma)$. Рассмотрим вывод $w$.<!-- .element: class="fragment" -->

Если в выводе используется длинное правило $A \rightarrow a_1 a_2 \ldots a_k$, то заменим его на последовательное применение правил $A \rightarrow a_1B_1$, $B_1 \rightarrow a_2B_2$, $B_2 \rightarrow a_3B_3$, $\ldots $, $B_{k-2} \rightarrow a_{k-1}a_{k}$.<!-- .element: class="fragment" -->

Получим вывод $w$ в $\Gamma'$.<!-- .element: class="fragment" -->

---

### Удаление длинных правил из КС-грамматики

$\Leftarrow $ 

Покажем, что $L(\Gamma') \subseteq L(\Gamma)$. 
Допустим, что это не так, то есть $\exists w \in L(\Gamma'), w \notin L(\Gamma)$. <!-- .element: class="fragment" -->

Рассмотрим вывод $w$ в $\Gamma' \cup \Gamma$, минимальный по количеству примененных правил, отсутствующих в $\Gamma$.<!-- .element: class="fragment" -->

Найдем в этом выводе первое применение некоторого правила $A \rightarrow a_1A_1, a_1 \in \Sigma \cup N$, которого нет в $\Gamma$. <!-- .element: class="fragment" -->

В ходе алгоритма оно было получено из некоторого длинного правила $A \rightarrow a_1 a_2 \ldots a_k$.<!-- .element: class="fragment" -->

Применим $A \rightarrow a_1 a_2 \ldots a_k$ вместо $A \rightarrow a_1A_1$ и удалим в выводе все применения правил, полученных из $A \rightarrow a_1 a_2 \ldots a_k$.<!-- .element: class="fragment" -->


Получим вывод $w$ в $\Gamma \cup \Gamma'$, в котором меньше применений правил, отсутствующих в $\Gamma$, чем в исходном.<!-- .element: class="fragment" --> 

Противоречие.<!-- .element: class="fragment" -->

Ч.т.д.<!-- .element: class="fragment" -->

---

### Удаление длинных правил из КС-грамматики

Покажем, как описанный алгоритм будет работать на следующей грамматике:
* $S \rightarrow AB$       
* $A \rightarrow aBcB$    
* $B \rightarrow def$. 

Для правила $A \rightarrow aBcB$ вводим $ 2 $ новых нетерминала $A_1, A_2$ и $ 3 $ новых правила:<!-- .element: class="fragment" -->
* $A \rightarrow aA_1$<!-- .element: class="fragment" -->
* $A_1 \rightarrow BA_2$<!-- .element: class="fragment" -->
* $A_2 \rightarrow cB$.<!-- .element: class="fragment" -->

---

### Удаление длинных правил из КС-грамматики

Для правила $B \rightarrow def$ вводим $ 1 $ новый нетерминал $B_1$ и $ 2 $ новых правила:
* $B \rightarrow dB_1$<!-- .element: class="fragment" -->
* $B_1 \rightarrow ef$.<!-- .element: class="fragment" -->

В итоге полученная грамматика $\Gamma'$ будет иметь вид:<!-- .element: class="fragment" -->
* $S \rightarrow AB$<!-- .element: class="fragment" -->
* $A \rightarrow aA_1$<!-- .element: class="fragment" -->
* $A_1 \rightarrow BA_2$ <!-- .element: class="fragment" -->
* $A_2 \rightarrow cB$ <!-- .element: class="fragment" -->
* $B \rightarrow dB_1$ <!-- .element: class="fragment" -->
* $B_1 \rightarrow ef$.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики

Цепное правило — правило вида $A→B$, где $A$ и $B$ — нетерминалы.

Цепная пара — упорядоченная пара $(A,B)$, в которой $A⇒^∗B$, используя только цепные правила.

---

### Удаление цепных правил из грамматики

Алгоритм удаления цепных правил из грамматики:

1. Найти все цепные пары в грамматике $Γ$.<!-- .element: class="fragment" -->
2. Для каждой цепной пары $(A,B)$ добавить в грамматику $Γ′$ все правила вида $A→α$, где $B→α$ — нецепное правило из $Γ$.<!-- .element: class="fragment" -->
3. Удалить все цепные правила<!-- .element: class="fragment" -->

Теорема: Если грамматика $Γ′$ была построена с помощью описанного выше алгоритма по грамматике $Γ$, то $L(Γ′)=L(Γ)$.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики

$\Rightarrow $<!-- .element: class="fragment" -->

Покажем, что $L(\Gamma) \subseteq L(\Gamma')$.<!-- .element: class="fragment" -->

Пусть $w\in L(\Gamma)$. Тогда $w$ имеет левое порождение $S\overset{\ast} {\underset{lm}{\Rightarrow}} w$.<!-- .element: class="fragment" -->

Где бы в левом порождении ни использовалось цепное правило, нетерминал в правой части становится крайним слева в выводимой цепочке и сразу же заменяется.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики


Таким образом, левое порождение в $\Gamma$ можно разбить на последовательность шагов, в которых ноль или несколько цепных правил сопровождаются нецепным. 

Заметим, что любое нецепное правило, перед которым нет цепных, образует такой шаг.<!-- .element: class="fragment" -->

Но по построению $\Gamma'$ каждый из этих шагов может быть выполнен одним её правилом.<!-- .element: class="fragment" -->

Таким образом, $S\overset{\ast}{\underset{\Gamma'}{\Rightarrow}} w$, то есть $w\in L(\Gamma')$.<!-- .element: class="fragment" -->


---

### Удаление цепных правил из грамматики

$\Leftarrow $ 

Покажем, что $L(\Gamma') \subseteq L(\Gamma)$.<!-- .element: class="fragment" -->

Пусть $w\in L(\Gamma')$, то есть $S\overset{\ast}{\underset{\Gamma'}{\Rightarrow}} w$.<!-- .element: class="fragment" -->

Так как каждое правило $\Gamma'$ эквивалентно последовательности из нуля или нескольких цепных правил $\Gamma$, за которой следует нецепное правило из $\Gamma$, то из $\alpha{\underset{\Gamma'}{\Rightarrow}} \beta$ следует $\alpha\overset{\ast}{\underset{\Gamma}{\Rightarrow}} \beta$.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики

Таким образом, каждый шаг порождения в $\Gamma'$ может быть заменен одним или несколькими шагами в $\Gamma$. 

Собрав эти последовательности шагов, получим, что $S\overset{\ast}{\underset{\Gamma}{\Rightarrow}} w$, то есть $w\in L(\Gamma)$.<!-- .element: class="fragment" -->

Ч.т.д.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики

Рассмотрим грамматику:

$$
\begin{array}{l l}
    A\rightarrow B|a \\\\
    B\rightarrow C|b \\\\
    C\rightarrow DD|c
\end{array}$$

Вкоторой есть два цепных правила $A\rightarrow B$ и $B\rightarrow C$.

1. Для каждого нетерминала создадим цепную пару. Теперь множество цепных пар будет состоять из $(A, A)$, $(B, B)$, $(C, C)$ и $(D, D)$.<!-- .element: class="fragment" -->
1. Рассмотрим цепное правило $A\rightarrow B$. Так как существует цепная пара $(A, A)$, второй элемент которой совпадает с левым нетерминалом из правила, добавим в множество пару $(A, B)$, у которой первый элемент такой же как у найденной, а второй равен правому нетерминалу из текущего правила.<!-- .element: class="fragment" -->

---

### Удаление цепных правил из грамматики


3. Повторим второй пункт для правила $B\rightarrow C$ и пары $(B, B)$. Теперь множество цепных пар будет состоять из $(A, A)$, $(B, B)$, $(C, C)$, $(D, D)$, $(A, B)$ и $(B, C)$.<!-- .element: class="fragment" -->
1. Повторим второй пункт для правила $B\rightarrow C$ и пары $(A, B)$, и получим множество $\lbrace (A, A), (B, B), (C, C), (D, D), (A, B), (B, C), (A, C)\rbrace$.<!-- .element: class="fragment" -->
1. Для каждой пары добавим в $\Gamma'$ новые правила:<!-- .element: class="fragment" -->
    * $A\rightarrow b$ для $(A, B)$<!-- .element: class="fragment" -->
    * $A\rightarrow c$ и $A\rightarrow DD$ для $(A, C)$<!-- .element: class="fragment" -->
    * $B\rightarrow c$ и $B\rightarrow DD$ для $(B, C)$<!-- .element: class="fragment" -->
    * Оставшиеся цепные пары новых правил не добавят.<!-- .element: class="fragment" -->

---

### Нормальная форма Хомского

Грамматикой в нормальной форме Хомского называется КС-грамматика, в которой содержатся правила только следующих видов:

* $A→BC$
* $A→a$
* $S→\lambda$

где $a$ — терминал, $A,B,C$ — нетерминалы, $S$ — стартовая вершина, $\lambda$ — пустая строка, стартовый нетерминал не содержится в правых частях правил.

---

### Нормальная форма Хомского

Теорема: Любую контекстно-свободную грамматику можно привести к нормальной форме Хомского.

Рассматорим КС-грамматику $\Gamma$ и применем к ней последовательно алгоритмы:
1. Добавить новый стартвый нетерминал
2. Удалить $\lambda$-правила
3. Удалить цепные правила
4. Удалить правила длины $>= 2$ с терминалами
5. Удалить правила длины $>= 3$ с нетерминалами
6. Удалим бесполезные символы

Пункты 1 означает добавления правила $S' \rightarrow S$.<!-- .element: class="fragment" -->

Для пункта 4, для всех правил вида $A→u_1u_2$ (где $u_i$ — терминал или нетерминал) заменим все терминалы $u_i$ на новые нетерминалы $U_i$ и добавим правила  $U_i→u_i$.<!-- .element: class="fragment" -->


---

### Нормальная форма Хомского

Приведём к нормальной форме Хомского грамматику:

* $S→aXbX|aZ$ 
* $X→aY|bY|\lambda$
* $Y→X|cc$
* $Z→ZX$

---

### Нормальная форма Хомского

Новый стартовый терминал и удаление $\lambda$ правил:

<div id="left">
<textarea id="simplify_1" class="editor" for="simplify_1">  

* $S→aXbX|aZ$ 
* $X→aY|bY|\lambda$
* $Y→X|cc$
* $Z→ZX$

</textarea>
</div>
<div id="right" class="simplify_1">

</div>

---

### Нормальная форма Хомского

Удаление цепных правила:

<div id="left">
<textarea id="simplify_2" class="editor" for="simplify_2">  

</textarea>
</div>
<div id="right" class="simplify_2">

</div>

---

### Нормальная форма Хомского

Удалить правила длины $>= 2$ с терминалами:

<div id="left">
<textarea id="simplify_3" class="editor" for="simplify_3"> 

</textarea>
</div>
<div id="right" class="simplify_3">

</div>

---

### Нормальная форма Хомского

Удалить правила длины $>= 3$ с нетерминалами:

<div id="left">
<textarea id="simplify_4" class="editor" for="simplify_4">  

</textarea>
</div>
<div id="right" class="simplify_4">

</div>

---

### Нормальная форма Хомского

Удалим бесполезные символы:

<div id="left">
<textarea id="simplify_5" class="editor" for="simplify_5"> 

</textarea>
</div>
<div id="right" class="simplify_5">

</div>

