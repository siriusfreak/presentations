---
title: "Лекция 11. Теория сложности"
block: "Теоретические модели вычислений 2022"
order: 11

---

## Теоретические модели вычислений. 
## Лекция 11. Теория сложности


#### 22 апреля 2022 года

---

### План лекции

1. Вычислительная сложность
2. Расчёт сложности алгоритмов
3. Классы сложности


---

### Понятие сложности алгоритмов

Вычисли́тельная сло́жность — понятие в информатике и теории алгоритмов, обозначающее функцию зависимости объёма ресусов, которые требуются некоторому алгоритму, от размера входных данных. 

Раздел, изучающий вычислительную сложность, называется теорией сложности вычислений. 

---

### Понятие сложности алгоритмов

Временная сложность алгоритма  — это функция от размера входных данных, равная максимальному количеству элементарных операций, проделываемых алгоритмом для решения экземпляра задачи указанного размера.

По аналогии с временной сложностью, определяют пространственную сложность алгоритма, только здесь говорят не о количестве элементарных операций, а об объёме используемой памяти. 

---

### Понятие сложности алгоритмов

Обычно определяют сложность не точно, а смотрят на её порядок.

$f(n) \in O(g(n))$

$f$ ограничена сверху функцией $g$ (с точностью до постоянного множителя) асимптотически

`$\exists (C>0), n_0 : \forall(n>n_0) \; |f(n)| \leq |Cg(n)|$` или `$\exists (C>0), n_0 : \forall(n>n_0) \; f(n) \leq Cg(n)$||$f(n) \in \Omega(g(n))$`

---

### Понятие сложности алгоритмов

$f(n) \in \Omega(g(n))$

$f$ ограничена снизу функцией $g$ (с точностью до постоянного множителя) асимптотически

`$\exists (C>0), n_0 : \forall (n>n_0) \; |Cg(n)| \leq |f(n)|$`


---

### Понятие сложности алгоритмов


$f(n) \in \Theta(g(n))$

$f$ ограничена снизу и сверху функцией $g$ асимптотически

`$\exists (C,C'>0), n_0 : \forall (n>n_0) \; |Cg(n)| \leq |f(n)| \leq |C'g(n)| $`

---

### Понятие сложности алгоритмов

$f(n) \in o(g(n))$

$g$ доминирует над $f$ асимптотически

`$\forall (C>0),\exists n_0 : \forall(n>n_0) \; |f(n)| < |Cg(n)|$`

---

### Понятие сложности алгоритмов

$f(n) \in \omega(g(n))$

$f$ доминирует над $g$ асимптотически 

`$\forall (C>0),\exists n_0 : \forall(n>n_0) \; |Cg(n)| < |f(n)|$`

---

### Понятие сложности алгоритмов

$f(n) \sim g(n)$

$f$ эквивалентна $g$ асимптотически


`$\lim_{n \to \infty} \frac{f(n)}{g(n)} = 1$`


---

### Расчёт сложности алгоритмов

Пусть $T_1(n)$ и $T_2(n)$ – время выполнения двух программных фрагментов $P_1$ и $P_2$. 

$T_1(n)$ имеет степень роста $O(f(n))$, а $T_2(n)$ – $O(g(n))$.  

Тогда $T_1(n) + T_2(n)$, т. е. время последовательного выполнения фрагментов $P_1$ и $P_2$, имеет степень роста $O( \operatorname{max} (f(n), g(n)))$.  

---

### Расчёт сложности алгоритмов

Правило сумм используется для вычисления времени последовательного выполнения программных фрагментов с циклами и ветвлениями. 

Предположим, что есть три фрагмента с временами выполнения соответственно $O(n^2)$, $O(n^3)$ и $O(n \log n)$.

Тогда время последовательного выполнения первых двух фрагментов име­ет порядок $O(max(n^2, n^3))$, т. е. $O(n^3)$. 

Время выполнения всех трех фрагментов имеет порядок $O(max(n^3, n \log n))$, это то же самое, что $O(n^3)$. 


---

### Расчёт сложности алгоритмов

Правило произведений заключается в следующем. Если $T_1(n)$ и $T_2(n)$  имеют сте­пени роста $O(f(n))$ и $O(g(n))$ соответственно, то произведение $T_1(n) \times T_2(n)$ имеет степень роста $O(f(n) \times g(n))$.  

Из пра­вила произведений следует, что $O(cf(n))$ эквивалентно $O(f(n))$, если $с$ – положитель­ная константа. Например, $O(\frac{n^2}{2})$ эквивалентно $O(n^2)$.

---

### Расчёт сложности алгоритмов

Для примера возьмём Heap-sort. Это сортировка кучей.

Необходимо отсортировать массив $A$, размером $n$. 

Построим на базе этого массива за $O(n)$ кучу для максимума. 

Так как максимальный элемент находится в корне, то если поменять его местами с $A_{n−1}$, он встанет на своё место. 

Далее за $O(\log n)$ просеет $A_{0}$ на нужное место и сформирует новую кучу (так как мы уменьшили её размер, то куча располагается с $A_0$ по $A_{n−2}$, а элемент $A_{n−1}$ находится на своём месте). 

---

### Расчёт сложности алгоритмов

Повторим эту процедуру для новой кучи, только корень будет менять местами не с $A_{n−1}$, а с $A_{n−2}$. 

Делая аналогичные действия, пока размер кучи не станет равен 1, мы будем ставить наибольшее из оставшихся чисел в конец не отсортированной части. 

Очевидно, что таким образом, мы получим отсортированный массив. 

---

### Расчёт сложности алгоритмов

Время работы алгоритма складывается из:

1. $O(n * \log n)$ - построение кучи 
2. $O(\log n)$ - перестроение кучи, при снятии верхнего элемента 
3. Нам надо перестроить кучу $n$ раз.

Итого: $O(n \log n) + O(n \log n) = O(n \log n)$ по времени. 

$O(1)$ по памяти, так как нет дополнительных расходов памяти. 

---

### Классы сложности

Классы сложности -- это классы эквивалентных по сложности задач. 

Они определяются относительно выполнения алгоритмов на машинах, подобных машине Тьюринга.

$T(m,x)$ — время работы машины Тьюринга $m$ на входе $x$.

$S(m,x)$ — объем памяти, требуемый машине Тьюринга $m$, для выполнения на входе $x$.

---

### Классы сложности

Классом $DTIME(f(n))$ называется множество языков, для которых существует машина Тьюринга такая, что она всегда останавливается, и время ее работы не превосходит $f(n)$, где $n$ — длина входа. 

$DSPACE(f(n))$ — класс языков $L$, для которых существует детерминированная машина Тьюринга m такая, что $L(m)=L$ и для любого $x$ выполнено $S(m,x)=O(f(n))$ (здесь $n$ — длина $x$).


---

### Классы сложности

Недетерминированная машина Тьюринга (НМТ) — машина Тьюринга, в которой существует пара "ленточный символ - состояние", для которой существует 2 и более команд.

Классом $NTIME(f)$ по аналогии с $DTIME$ называется класс языков(задач), для которых существует недетерминированная машина Тьюринга, такая, что она всегда останавливается, и время ее работы не превосходит $f(n)$, где $n$ - длина входа. 

$NSPACE(f(n))$ — класс языков $L$, для которых существует НМТ $m$ такая, что $L(m)=L$ и для любого $x$ выполнено $S(m,x)=O(f(n))$ (здесь n — длина x).


---

### Классы сложности

Определения выше задают по факту пространство и время в терминах теории вычислений. 

То есть ли в физике мы изучаем материю и её формы, то здесь мы приходим к тому, что у нас важен алгоритм.

---

### Класс $P$

Класс $P$ — класс языков (задач), разрешимых на детерминированной машине Тьюринга за полиномиальное время, то есть: $P=\bigcup\limits_{p \in poly}DTIME(p(n))$.

Итого, язык $L$ лежит в классе $P$ тогда и только тогда, когда существует такая детерминированная машина Тьюринга $M$, что: 

* $M$ завершает свою работу за полиномиальное время на любых входных данных; 
* если на вход машине m подать слово $l∈L$, то она допустит его;
* если на вход машине m подать слово $l∉L$, то она не допустит его. 

---

### Класс P

Машина Тьюринга может симулировать другие модели вычислений (например, языки программирования) с не более чем полиномиальным замедлением. Благодаря этому, класс $P$ на этих моделях не становится шире.

Согласно тезису Чёрча-Тьюринга, любой физически реализуемый алгоритм можно реализовать на машине Тьюринга. Так что класс P устойчив и в обратном преобразовании модели вычислений.

---

### Класс $P$

Класс задач, разрешимых за полиномиальное время достаточно широк, вот несколько его представителей:

* определение связности графов; 
* вычисление наибольшего общего делителя; 
* задача линейного программирования; 
* проверка простоты числа. 

---

### Класс $P$

Как мы можем сказать, что касс языков входит в класс сложности?

Класс регулярных языков входит в класс $P$, то есть: $Reg ⊂ P$.

Класс контекстно-свободных языков входит в класс $P$, то есть: $CFL⊂P$. 

---

### Класс $NP$

В теории сложности Класс $NP$ — класс языков (задач), класс задач, ответ на которые можно найти за полиномиальное время на недетерминированной МТ.

$$NP=\bigcup\limits_{p \in poly}NTIME(p(n))$$.

---

### Класс $coNP$

Класс $coNP$ — это множество языков, дополнение к которым лежит в $NP$.

$coNP=\\{L∣\overline{L}∈NP\\}$

---

### Класс $Σ_1$

$Σ_1$ — это множество языков, для которых существует работающая за полиномиальное время детерминированная программа-верификатор $R(x,y)$.

```$$\mathrm{\Sigma_1}=\{L\bigm|\exists R(x,y)\in \tilde{\mathrm{P}}, p(n) \in \mathit{poly} : x\in L\Leftrightarrow\exists y : |y|\leqslant p(|x|), R(x,y)=1\}$$```

Для каждого слова из языка (и только для слова из языка) можно предъявить сертификат $y$ полиномиальной длины, подтверждающий принадлежность слова языку и проверяемый верификатором.

---

### Класс $Π_1$

$Π_1$ — это множество языков, для которых существует работающая за полиномиальное время детерминированная программа-верификатор $R(x,y)$,

```$$\mathrm{\Pi_1}=\{L\bigm|\exists R(x,y)\in \tilde{\mathrm{P}}, p(n) \in \mathit{poly} : x\in L\Leftrightarrow\forall y : |y|\leqslant p(|x|), R(x,y)=1\}$$```

Для каждого слова из языка (и только для слова из языка) нельзя предъявить сертификат $y$ длины, ограниченной неким полиномом, опровергающий принадлежность слова языку и проверяемый верификатором. 

$Π_1$ дополнение к $Σ_1$

---

### Равенство $\mathrm{\Sigma_1}=\mathrm{NP}$

$\Rightarrow \mathrm{\Sigma_1} \subset \mathrm{NP}$

Пусть $L∈Σ_1$. Тогда существуют $R(x,y)$ и полином $p$ из определения $Σ_1$. Построим недетерминированную программу $q(x)$, разрешающую $L$.

```
q(x):
  y={0,1}^p(|x|)
  return R(x,y)
```

Эта программа перебором ищет подходящий $y$.

---

### Равенство $\mathrm{\Sigma_1}=\mathrm{NP}$

Если $x∈L$, то программа сможет «угадать» подходящий сертификат. Если $x∉L$, то подходящего сертификата не существует по определению. Таким образом, $q$ разрешает $L$, следовательно $L\subset NP$.


---

### Равенство $\mathrm{\Sigma_1}=\mathrm{NP}$

$⇐ NP⊂Σ_1$

Пусть $L∈NP$. Тогда существует недетерминированная программа $q(x)$, разрешающая этот язык. Построим верификатор $R(x,y)$.

В качестве сертификата будем использовать последовательность выборов в программе $q$, приводящую к допуску слова. Длина сертификата не более чем полиномиальная, так как выборов в $q$ можно сделать не более чем полином.

---

### Равенство $\mathrm{\Sigma_1}=\mathrm{NP}$

Верификатор будет аналогичен программе $q$, только вместо каждого недетерминированного выбора он будет присваивать значение, указанное в сертификате. 

Если $x∈L$, то в $q$ существует последовательность выборов таких, что $q(x)=1$, следовательно существует и верный сертификат.

Если $x∉L$, то для любой последовательности выборов $q(x)=0$, следовательно подходящего сертификата не существует. Таким образом, $L∈Σ_1$.

---

### Свойства $NP$

Пусть $L_1,L_2∈NP$. Тогда:
1. $L_1∩L_2∈NP$
1. $L_1∪L_2∈NP$
1. $L_1L_2∈NP$
1. $L^∗_1∈NP$

---

### Классы сложности

Можно привести много задач, про которые на сегодняшний день неизвестно, принадлежат ли они P, но известно, что они принадлежат NP. Среди них:

* Задача выполнимости булевых формул: узнать по данной булевой формуле, существует ли набор входящих в неё переменных, обращающий её в 1. Сертификат — такой набор. 
* Задача о клике: по данному графу узнать, есть ли в нём клики (полные подграфы) заданного размера. Сертификат — номера вершин, образующих клику. 

---

### Классы сложности

* Определение наличия в графе гамильтонова цикла. Сертификат — последовательность вершин, образующих гамильтонов цикл.
* Неоптимизационный вариант задачи о коммивояжёре (существует ли маршрут не длиннее, чем заданное значение k) — расширенный и более приближенный к реальности вариант предыдущей задачи. Сертификат - такой маршрут. 
* Существование целочисленного решения у заданной системы линейных неравенств. Сертификат — решение.

---

### Классы сложности

$NP$-полная задача — в теории алгоритмов задача с ответом «да» или «нет» из класса $NP$, к которой можно свести любую другую задачу из этого класса за полиноминальное время (то есть при помощи операций, число которых не превышает некоторого полинома в зависимости от размера исходных данных).  

---

### Классы сложности


Таким образом, $NP$-полные задачи образуют в некотором смысле подмножество «типовых» задач в классе $NP$: если для какой-то из них найден «полиномиально быстрый» алгоритм решения, то и любая другая задача из класса $NP$ может быть решена так же «быстро». 

Все остальные задачи класса $NP$ называются $NP$-трудними. 

---

### Классы сложности

Проблема равенства $P = NP$ состоит в следующем: если положительный ответ на какой-то вопрос можно довольно быстро проверить (за полиномиальное время), то правда ли, что ответ на этот вопрос можно довольно быстро найти (также за полиномиальное время и используя полиномиальную память)?  

Другими словами, действительно ли решение задачи проверить не легче, чем его отыскать? <!-- .element: class="fragment" -->

---

### Класс PS (PSPACE)

$PS$ -- класс языков, разрешимых на детерминированной машине Тьюринга с использованием памяти полиномиального размера.

$$\mathrm{PS}=\bigcup\limits_{p(n) \in poly} \mathrm{DSPACE}(p(n))$$

---

### Класс NPS (NPSPACE)

$NPS$ — класс языков, разрешимых на недетерминированной машине Тьюринга с использованием памяти полиномиального размера.

$$\mathrm{PS}=\bigcup\limits_{p(n) \in poly} \mathrm{NSPACE}(p(n))$$

---

### Теорема Сэвича

NPSPACE эквивалентна PSPACE, так как детерминированная машина Тьюринга может имитировать недетерминированную машину Тьюринга, не требуя гораздо большего пространства (даже если это может занять гораздо больше времени). 

---

### Иерархия классов сложности


<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Complexity_subsets_pspace.svg/1920px-Complexity_subsets_pspace.svg.png" height="500px">
