---
customTheme : "presentation"

---

## Лекция 4. Интерфейсы, моки и тестирование с ними
### OZON

### Москва, 2021


---

### Лекции

1. <span style="color:gray">Введение. Рабочее окружение. Структура программы. Инструментарий.</span>
2. <span style="color:gray">Базовые конструкции и операторы. Встроенные типы и структуры данных.</span>
3. <span style="color:gray">Структуры данных, отложенные вызовы, обработка ошибок и основы тестирования</span>
4. Интерфейсы, моки и тестирование с ними
5. <span style="color:gray">Асинхронные сущности и паттерны в Go</span>
6. <span style="color:gray">Protobuf и gRPC</span>
7. <span style="color:gray">Работа с БД в Go</span>
8. <span style="color:gray">Брокеры сообщений. Трассировка. Метрики. </span>

---

### Темы

Сегодня мы поговорим про:

1. Что такое Интерфейс в Go и зачем он нужен? <!-- .element: class="fragment" -->
1. Как разрабатывать классы  <!-- .element: class="fragment" -->
1. Генерация моков <!-- .element: class="fragment" -->
1. Тесирование с моками <!-- .element: class="fragment" -->

---

### Обозначения

* 📽️ - посмотри воркшоу
* ⚗️ - проведи эксперимент
* 🔬 - изучи внимательно
* 📖 - прочитай документация
* 🪙 - подумай о сложности
* 🐞 - запомни ошибку
* 🔨 - запомни решение
* 🏔️ - обойди камень предкновенья
* ⏰ - сделай перерыв
* 🏡 - попробуй дома
* 💡 - обсуди светлые идеи
* 🙋 - задай вопрос
* ⚡ - запомни панику

---

### Определение

> An **interface type** is defined as a set of method signatures
>
> A **value** of interface type can hold any value that implements those methods

```go
type Stringer interface {
  String() string
}

func (u *User) String() string {
  if u == nil {
  	return u.Name
  }
}

type User struct {
	Name string
}

func foo(s Stringer) {
  fmt.Println(s.String())
}

func main(){
  user := &User{"Anna"}
  foo(user)
}
```

---

### Определение

Количество методов:

- один - суффиксы - `er`,` or`
- больше одного


The static type (or just type) of a variable is the type given in its declaration, the type provided in the new call or composite literal, or the type of an element of a structured variable.

Variables of interface type also have a distinct dynamic type, which is the concrete type of the value assigned to the variable at run time (unless the value is the predeclared identifier nil, which has no type). 

---

### Что под капотом

```go
 stringer                                    user
+--------+        +--------------+         +-----+
| data   |------->| Name: "Rita" |<--------| ptr |
+--------+        +--------------+         +-----+
| itable |
+--------+
    |
    |                  +---------+         +--------+
    +----------------->| type    |-------->| *User  |
                       +---------+         +--------+
                       | ...     |
                       +---------+         +--------------+
                       | func[0] |-------->| *User.String |
                       +---------+         +--------------+


func main() {
	user := &User{"Rita"}
	var stringer Stringer
	stringer = user
	user.Name = "Masha"
	fmt.Println(stringer)
}
```

- Статический тип - `Stringer`

- Динамический тип - `User`

- Значение динамичегоского типа - `user`

- Значение типа интерфейса - `stringer`

---

### Что под капотом

- Что будет  `user := User{"Rita"}` 🏡

---

### Равенство **nil**

```go
 stringer                                    user
+--------+                                 +-----+
| nil    |                                 | nil |
+--------+                                 +-----+
| nil    |
+--------+


func main() {
	var user *User
	var stringer Stringer
  if stringer == nil {
    fmt.Println("stringer is nil")
  }
}
```

- Статический тип - `Stringer`
- Динамический тип - `nil`
- Значение динамичегоского типа - `nil`
- Значение типа интерфейса - `nil`

---

### Равенство **nil**

```go
 stringer                                    user
+--------+                                 +-----+
| nil    |                                 | nil |
+--------+                                 +-----+
| itable |
+--------+
    |
    |                  +---------+         +--------+
    +----------------->| type    |-------->| *User  |
                       +---------+         +--------+
                       | ...     |
                       +---------+         +--------------+
                       | func[0] |-------->| *User.String |
                       +---------+         +--------------+

func main() {
	var user *User // nil
	var stringer Stringer
	stringer = user
  if stringer != nil {
    fmt.Println("stringer isn't nil")
  }
}
```

---

### Равенство **nil**

- Статический тип - `Stringer`
- Динамический тип - `User`
- Значение динамичегоского типа - `nil`
- Значение типа интерфейса - `stringer`

> interface type value is **nil** if both dynamic value and dynamic type are **nil**


---

### Наследование

```go
type Reader interface {
  Read(b []byte) (n int, err error)
}

type Closer interface {
  Сlose()
}

type ReadCloser interface {
  Reader
  Closer
}
```
Дополнительные методы


---

### Наследование

Пересечение методов

- Наименования и аргументы совпадают
- Совпадают только наименования
- Полностью совпадают

Наследование уже существующего - пример для стандартной библиотеки


```go
import "fmt"

type Bar interface {
  fmt.Stringer
}
```

---

### Конструирование

Добавим интерфейс **Repo** для работы с хранилищем данных

```go
package repo

type Repo interface {
	AddTask(task models.Task) (uint64, error)
}
```

Добавим интерфейс **Flusher** для сброса задач в хранилище данных

```go
package flusher

type Flusher interface {
	Flush(tasks []models.Task) []models.Task
}

type flusher struct {
  repo repo.Repo
}

func New(repo repo.Repo) Flusher {
  return &flusher{
    repo: repo
  }
}
```

---

### Конструирование

**New** - для конструирования с защитой от пустого динамического значения

Имя структуры не экспортируется для инкапсуляции

При необходимости для инициализации добавляются методы:

- Init
- Close

💡 Стоит ли делать проверку на **nil** для **New**, коротрые возвращают интерфейс?

---

### Стандартная библиотека

```go
type error interface {
    Error() string
}

type Stringer interface {
    String() string
}
type ReadWriteCloser interface {
    Reader
    Writer
    Closer
}

type Handler interface {
    ServeHTTP(ResponseWriter, *Request)
}

//...
```

---

### Стандартная библиотека

Список интерфейсов из стандартной библиотеки:

- `error`
- `fmt.Stringer`
- `io.Reader`, `io.Writer`, `io.Closer`, `io.ReadWriteCloser`
- `http.Handler`, `http.RoundTripper`
- `sort.Interface`
- `heap.Interface`
- `net.Addr`, `net.Conn`, `net.Listener`, `net.Error`

Попробуй каждый интерфейс в свободное время 🏡

---

### Действия с интерфейсами

Пустой интерфейс

```go
interface{}

func foo(a interface{})
```
Приведение к интерфейсу

```go
t := i.(T) // ⚡ If i does not hold a T, the statement will trigger a panic.
t, ok := i.(T) // If i does not hold a T, the statement will not trigger a panic.
```

---

### Switch

```go
package main

import "fmt"

type StatusType uint

const (
	StatusRunning StatusType = iota
	StatusStopped
)

// 1 -> Running <- "running"
// 0 -> Stopped <- "stopped"
func ...(status interface{}) (StatusType, error) {
	switch v := status.(type) {
	case int:
		//...
	case string:
		//...
	default:
    return newErrInvalidStatus(status)
	}
}
```

---

### Примеры

Пакет для работы с хранилищем

```go
package repo

type Repo interface {
  AddTasks(task []models.Task) error
  RemoveTask(taskId uint64) error
  DescribeTask(taskId uint64) (*models.Task, error)
 	ListTasks(limit, offset uint64) ([]models.Task, error)
}
```

Пакет для отправкии данных в хранилище

```go
package flusher

type Flusher interface {
	Flush(tasks []models.Task) []models.Task
}
```

---

### Пакет для работы с хранилищем

Пакет для публикации метрик

```go
package metrics

type Publisher interface {
	PublishFlushing(count int)
}
```

internal

- flusher
- metrics
- publisher


Имплементация **internal/flusher/flusher.go**

---

### Перерыв


⏰&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;⏰&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;⏰



---

### Ginkgo

```sh
➜ go get github.com/onsi/ginkgo/ginkgo # установка ginkgo
```

```shell
➜ cd internal/flusher 
➜ ginkgo bootstrap 				 # создаем flusher_suite_test.go
➜ ginkgo generate flusher  # генерируем flusher_test.go
➜ ginkgo 									 # запускаем тестовые сценарии
```

Содержимое файла **flusher_suite_test.go**:

```go
package flusher_test

import (
	"testing"
	. "github.com/onsi/ginkgo"
)

func TestFlusher(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Flusher Suite")
}
```

Содержимое файла **flusher_test.go**:

```go
package flusher_test

import (
	. "github.com/onsi/ginkgo"
	"github.com/ozoncp/ocp-task-api/internal/flusher"
)

var _ = Describe("Flusher", func() {
  
})
```


---

### Ginkgo

```go
BeforeEach(func() {
	// ...  
})

JustBeforeEach(func(){
  // ...  
})

AfterEach(func(){
})

Context("", func() {
  BeforeEach(func() { 
    // ...
  })
  
  JustBeforeEach(func(){
    // ...
	})
  
  It("", func() {
    // ...
  ))
})
```

1. `BeforeEach`
2. `JustBeforeEach`
3. `JustAfterEach`
4. `AfterEach`

---

### Ginkgo

1. `BeforeSuite`
2. `AfterSuite`

---

### Порядок вызовов

```go
BeforeEach(func() {
	// ...1️⃣
})

JustBeforeEach(func(){
  // ...3️⃣
})

AfterEach(func(){
  // ...6️⃣
})

Context("case", func() {
  BeforeEach(func() { 
    // ...2️⃣
  })
  
  JustBeforeEach(func(){
    // ...4️⃣
	})
  
  It("do", func() {
    // ...5️⃣
  ))
})
```

```go
var _ = BeforeSuite(func() {
  // ...0️⃣
}
                    
var _ = AfterSuite(func() {
  // ...7️⃣
})
```

---

### Gomega

```sh
➜ go get github.com/onsi/gomega/...
```

Установка только пакетов необходимых для использования **gomega**

```go
import "github.com/onsi/gomega"

Expect(5.1).Should(BeEquivalentTo(5))
Expect(5).ShouldNot(BeEquivalentTo(5.1))
```

---

### Gomega

|                                                              |                       |
| ------------------------------------------------------------ | --------------------- |
| Should / To, ShouldNot / ToNot                               |                       |
| Equal, BeEquivalentTo, BeIdenticalTo, BeAssignableToTypeOf   | Equivalence           |
| BeNil, BeZero, BeTrue, BeFalse                               | Presence / Truthiness |
| HaveOccurred, Succeed, MatchError                            | Error                 |
| BeEmpty, HaveLen, HaveCap, ContainElement / ContainElements, ConsistOf, HaveKey, HaveKeyWithValue | Collection            |
| Panic, PanicWith                                             | Panic                 |
| ContainSubstring, HavePrefix, HaveSuffix, MatchRegexp, MatchJSON, MatchYAML | String, Yaml, Json    |
| And / SatisfyAll, Or / SatisfyAny, WithTransform             | Composing Matchers    |
| ...                                                          | ...                   |


---

### Gomega

- Добавление анотаций
- Ассинхронные утверждения `Eventually`, `TIMEOUT`, `POLLING_INTERVAL`
- Пользовательские матчеры 
- Тестирование HTTP клиентов - **ghttp**
- Тестирование внешних процессов - **gexec**
- Упрощает тестирование больших и вложенных структур и срезов - **gstruct**

> Note that `Should` and `To` are just syntactic sugar and are functionally identical. Same is the case for `ToNot` and `NotTo`

---

### Mockgen

```sh
➜ go install github.com/golang/mock/mockgen@v1.5.0 # установка
```

Добавляем в **internal/mockgen.go** список интерфесов

```go
package internal

//go:generate mockgen -destination=./mocks/flusher_mock.go -package=mocks github.com/ozoncp/ocp-task-api/internal/flusher Flusher

//...github.com/ozoncp/ocp-task-api/internal/repo Repo

//...github.com/ozoncp/ocp-task-api/internal/metrics Publisher
```

```sh
➜ mkdir internal/mocks
➜ go genereate ./...
```

```shell
➜ tree internal/mocks
```

---

### Mockgen

Возможности:

- Переопределить имя мока
- Указать выходное имя файла
- Переопределить пакет
- Генерировать моки не только для локальных сущностей
- Поддержка двух режимов: **source mode** и **reflect mode**
- Поддержка dry run флагов и возможности добавления **Copyright** заголовка

---

### Пример

```go
import (
	"github.com/ozoncp/ocp-task-api/internal/mocks"
)
var _ = Describe("Flusher", func() {

	var (
		ctrl *gomock.Controller
    mockRepo      *mocks.MockRepo
		mockPublisher *mocks.MockPublisher
  )
  
  BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())

		mockRepo = mocks.NewMockRepo(ctrl)
		mockPublisher = mocks.NewMockPublisher(ctrl)
	})
  
  AfterEach(func() {
		ctrl.Finish()
	})

	Context("", func() {
    BeforeEach(func() {
		})
		It("", func() {
		})
	})
})

```

---

### Пример

```go
mockRepo.EXPECT().
	AddTasks(gomock.Any(), gomock.Any()).
	Return(nil)

mockPublisher.EXPECT().
	PublishFlushing(gomock.Any())
```

```go
func (c *Call) After(preReq *Call) *Call
func (c *Call) AnyTimes() *Call
func (c *Call) Do(f interface{}) *Call
func (c *Call) DoAndReturn(f interface{}) *Call
func (c *Call) MaxTimes(n int) *Call
func (c *Call) MinTimes(n int) *Call
func (c *Call) Return(rets ...interface{}) *Call
func (c *Call) SetArg(n int, value interface{}) *Call
func (c *Call) String() string
func (c *Call) Times(n int) *Call
```