const express = require('express')
const app = express()
const port = 3000
const path = require('path');
let ejs = require('ejs');
const puppeteer = require('puppeteer')
var bodyParser = require('body-parser');
const marked = require('marked');
const fs = require('fs');
const { count } = require('console');
const YAML = require('yaml');

const { join } = require('path');
const { readdirSync, statSync, readFileSync } = require('fs');


marked.setOptions({
  renderer: new marked.Renderer(),
  highlight: function(code, language) {
    const hljs = require('highlight.js');
    const validLanguage = hljs.getLanguage(language) ? language : 'plaintext';
    return hljs.highlight(validLanguage, code).value;
  },
  pedantic: false,
  gfm: true,
  breaks: false,
  sanitize: false,
  smartLists: true,
  smartypants: false,
  xhtml: false
});


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/reveal', express.static(path.join(__dirname, 'node_modules/reveal.js')))
app.use('/jquery', express.static(path.join(__dirname, 'node_modules/jquery/dist')))
app.use('/presentations', express.static(path.join(__dirname, 'presentations')))
app.use('/images', express.static(path.join(__dirname, 'presentations/images')))
app.use('/my_modules', express.static(path.join(__dirname, 'my_modules')))
app.use('/css', express.static(path.join(__dirname, 'css')))
app.use('/reveal-plugins', express.static(path.join(__dirname, 'node_modules/reveal.js-plugins')))


const getAllFiles = (dir, extn, files, result, regex) => {
  files = files || readdirSync(dir);
  result = result || [];
  regex = regex || new RegExp(`\\${extn}$`)

  for (let i = 0; i < files.length; i++) {
      let file = join(dir, files[i]);
      if (statSync(file).isDirectory()) {
          try {
              result = getAllFiles(file, extn, readdirSync(file), result, regex);
          } catch (error) {
              continue;
          }
      } else {
          if (regex.test(file)) {
              result.push(file);
          }
      }
  }
  return result;
}

function getYamlFromMD(md_file_data) {
  try {
    const regex = /---((?!---)(.|[\r\n]))*---/g;
    const found = md_file_data.match(regex);   
    if (found && found.length > 0) {
      yaml_str = found[0].replace("---", "").replace("---", "");
      yaml = YAML.parse(yaml_str);
      if (yaml.title && yaml.block) {
        if (!yaml.order)
          yaml.order = 666
        return yaml
      }
    }
  } catch (error) {
    console.log(error);
  }
}


app.get('/', (req, res) => {
    var blocks = {};
    var full_path = path.join(__dirname, 'presentations');
    var presentations = getAllFiles(full_path, ".md");

    for (let i = 0; i < presentations.length; i++) {
      let pr = readFileSync(presentations[i], 'utf8')
      if (pr) {
        let cur_yaml = getYamlFromMD(pr);
        if (cur_yaml) {
          let cur = {
            title: cur_yaml.title,
            filename: path.basename(presentations[i]),
            order: cur_yaml.order
          };

          if (blocks[cur_yaml.block]) {
            blocks[cur_yaml.block].push(cur)
          }
          else {
            blocks[cur_yaml.block] = [cur]
          }
        }
      }
    }

    for (let item in blocks) {
      blocks[item] = blocks[item].sort(function (a, b) {
        return a.order - b.order;
      })
    }

    ejs.renderFile('index.ejs', {blocks: blocks,}, function(err, str){
      console.log(err)
      return res.send(str)
     });
})


app.post('/load_render', (req, res) => {
  var filename = req.body.md_file + "_" + req.body.editor_id + ".md";
  res.sendFile(path.join(__dirname, 'saved', filename))
})

app.post('/render', (req, res) => {
  var text = req.body.text;
  var filename = req.body.md_file + "_" + req.body.editor_id + ".md";

  fs.writeFile(path.join(__dirname, 'saved', filename), text, function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("The file was saved!");
}); 

  require('mathjax').init({
    loader: {load: ['input/tex', 'output/svg']}
  }).then((MathJax) => {
    text = marked(text)
    var re = /\$(.*?)\$/g
    do {
      m = re.exec(text);
      if (m) {
        var svg = MathJax.tex2svg(m[1], {display: true});
        text = text.replace(m[0], MathJax.startup.adaptor.outerHTML(svg));
      }
    } while (m);   
    res.send(text);
  }).catch((err) => console.log(err.message));
})

app.post('/load_render', (req, res) => {
  var filename = req.body.md_file + "_" + req.body.editor_id + ".md";
  res.sendFile(path.join(__dirname, 'saved', filename))
})

app.get('/presentation/:presentation_name', function (req, res) {
  var full_path = path.join(__dirname, 'presentations', req.params.presentation_name) 
  var title = "";
    fs.readFile(full_path, 'utf8', function(err, data) {
        if (err) {
          console.log(err); 
        } else {
    
        yaml = getYamlFromMD(data);
        if (yaml) {
          if (yaml.title) {
            title = yaml.title;
          }
        }
      
       ejs.renderFile('presentation.ejs', {title: title, presentation_name: req.params.presentation_name}, {}, function(err, str){
        return res.send(str)
       });
    }

  });
})

app.get('/pdf/:presentation_name' , function (req, res) {
  res.sendFile(path.join(__dirname, "pdf",  req.params.presentation_name.replace(".md", ".pdf")))
})


async function printPDF(presentation_name) {
  const browser = await puppeteer.launch({ headless: true,  args: ['--no-sandbox'] });
  const page = await browser.newPage();
  await page.goto('http://127.0.0.1:3000/presentation/'+presentation_name+'?print-pdf', 
    {waitUntil: 'networkidle2'});
  const pdf = await page.pdf({ format: 'A3' });

  await browser.close();
  return pdf
}


app.listen(port, async () => {
  (async () => {
    console.log("Pregenerating pdfs")

    var files = fs.readdirSync("presentations/");
    for (var i = 0; i < files.length; i++) {
      var file = files[i]
      if (file.indexOf(".md") !== -1) {
        console.log(file);
        pdf_file = path.join(__dirname, 'pdf', file.replace(".md", ".pdf"))
        if (fs.existsSync(pdf_file)) {
          continue
        }

        pdf = await printPDF(file);
        fs.writeFile(pdf_file, pdf, function(err) {
          if(err) {
              return console.log(err);
          }
          console.log("The file was saved!");
        }); 
      }
    }
  })()
  console.log(`Example app listening at http://localhost:${port}`)
})
