/**
 * A plugin which enables rendering of math equations inside
 * of reveal.js slides. Essentially a thin wrapper for MathJax.
 *
 * @author Hakim El Hattab
 */
var RevealMath3 = window.RevealMath3 || (function(){

	var options = Reveal.getConfig().math || {};
	var mathjax = options.mathjax || 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js';
	var config = options.config || 'TeX-AMS_HTML-full';
	var url = mathjax + '?config=' + config;

	var defaultOptions = {
        tex: {
            inlineMath: [ [ '$', '$' ], [ '\\(', '\\)' ]  ]
        },
        options: {
            skipHtmlTags: [ 'script', 'noscript', 'style', 'textarea', 'pre' ]
        },
        startup: {
            ready: () => {
                MathJax.startup.defaultReady();
                MathJax.startup.promise.then(() => {
                    Reveal.layout();
                });
            }
        }
	};

	function defaults( options, defaultOptions ) {

		for ( var i in defaultOptions ) {
			if ( !options.hasOwnProperty( i ) ) {
				options[i] = defaultOptions[i];
			}
		}

	}

	function loadScript( url, callback ) {

        let script = document.createElement( 'script' );
        script.type = "text/javascript"
        script.id = "MathJax-script"
        script.src = url;
        script.async = true

        // Wrapper for callback to make sure it only fires once
        script.onload = () => {
            if (typeof callback === 'function') {
                callback.call();
                callback = null;
            }
        };

        document.head.appendChild( script );

	}

	return {
		init: function(reveal) {

			defaults( options, defaultOptions );
			defaults( options.tex2jax, defaultOptions.tex2jax );
			options.mathjax = options.config = null;
			window.MathJax = options;


			loadScript( url, function() {

			

				// Reprocess equations in slides when they turn visible
				Reveal.addEventListener( 'slidechanged', function( event ) {

					MathJax.typeset();

				} );

			} );

		}
	}

})();

Reveal.registerPlugin( 'math3', RevealMath3 );
